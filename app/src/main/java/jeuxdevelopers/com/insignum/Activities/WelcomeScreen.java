package jeuxdevelopers.com.insignum.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import jeuxdevelopers.com.insignum.R;

public class WelcomeScreen extends AppCompatActivity {

    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;

    int customerId = 0;

    Button loginBtn, registerBtn, signInLaterBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        loginBtn = findViewById(R.id.loginBtn);
        registerBtn = findViewById(R.id.registerBtn);
        signInLaterBtn = findViewById(R.id.laterBtn);

        customerId = appShared.getInt("customerId", 0);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WelcomeScreen.this, LoginActivity.class));
                //finish();
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WelcomeScreen.this, RegisterActivity.class));
                //finish();
            }
        });

        signInLaterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                appSharedEditor.putBoolean("signInLater", true);
                appSharedEditor.apply();

                startActivity(new Intent(WelcomeScreen.this, MainActivity.class));
                finish();
            }
        });
    }
}
