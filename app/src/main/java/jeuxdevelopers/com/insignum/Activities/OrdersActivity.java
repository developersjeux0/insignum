package jeuxdevelopers.com.insignum.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import jeuxdevelopers.com.insignum.Adapters.OrderAdapter;
import jeuxdevelopers.com.insignum.Config.Config;
import jeuxdevelopers.com.insignum.Models.OrderModel;
import jeuxdevelopers.com.insignum.Models.OrderProductModel;
import jeuxdevelopers.com.insignum.R;
import jeuxdevelopers.com.insignum.Universal.APIsInfo;

public class OrdersActivity extends AppCompatActivity {

    int customerId = 0;

    String ORDERURL = "https://insignum.hr/wp-json/wc/v3/orders?consumer_key=ck_55aa3f4a157322610059faf4beb4f61f3b08cc64&consumer_secret=cs_3e5dbc594f5543838dd6ed0dd96f41fa7ff89d0d&customer=";

    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;

    RecyclerView ordersRecycer;
    OrderAdapter orderAdapter;
    List<OrderModel> ordersList = new ArrayList<>();

    AVLoadingIndicatorView progressBar;

    ProgressDialog progressDialog;

    OrderModel currentProcessingModel;

    //setting and getting main activity
    private static OrdersActivity ordersActivity;

    public static OrdersActivity getOrdersActivity() {
        return ordersActivity;
    }

    private static void setOrdersActivity(OrdersActivity ordersActivity) {
        OrdersActivity.ordersActivity = ordersActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        OrdersActivity.setOrdersActivity(this);
        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        showLoadingDialog();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Processing Payment");
        progressDialog.setMessage("Please Wait while we process your payment. Please Don't close the application");

        ordersRecycer = findViewById(R.id.orders_recycler);
        orderAdapter = new OrderAdapter(ordersList, this);
        ordersRecycer.setAdapter(orderAdapter);
        ordersRecycer.setLayoutManager(new LinearLayoutManager(this));

        customerId = appShared.getInt("customerId", 0);
        
        if (customerId == 0){
            Toast.makeText(ordersActivity, "You need to login before checking your orders.", Toast.LENGTH_SHORT).show();
            finish();
        }

        Log.v("CustomerId__", "ID : " + customerId);

        ORDERURL = ORDERURL + customerId;

        progressBar = findViewById(R.id.avi);
        progressBar.setVisibility(View.VISIBLE);

        getData();
    }

    private void getData(){
        if (customerId != 0){

            ordersList.clear();

            final StringRequest jsonStringRequestForOrders = new StringRequest(Request.Method.GET, ORDERURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        Log.v("GetAllOrdersData__", "Started");

                        JSONArray jsonArray = new JSONArray(response);

                        for (int i = 0; i < jsonArray.length(); i++){

                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            int orderId = jsonObject.optInt("id");
                            String orderNumber = jsonObject.optString("number");
                            String orderKey = jsonObject.optString("order_key");
                            String status = jsonObject.optString("status");
                            String currency = jsonObject.optString("currency");
                            String dateCreated = jsonObject.optString("date_created");
                            String totalOrderPrice = jsonObject.optString("total");
                            int customerId = jsonObject.optInt("customer_id");
                            JSONObject billingObject = jsonObject.optJSONObject("billing");
                            JSONObject shippingObject = jsonObject.optJSONObject("shipping");
                            String paymentMethod = jsonObject.optString("payment_method");
                            String paymentTitle = jsonObject.optString("payment_method_title");
                            String trasactionId = jsonObject.optString("transaction_id");
                            String datePaid = jsonObject.optString("date_paid");

                            JSONArray itemsArray = jsonObject.getJSONArray("line_items");

                            List<OrderProductModel> productsList = new ArrayList<>();

                            for (int j = 0; j < itemsArray.length(); j++){
                                JSONObject productObject = itemsArray.getJSONObject(j);

                                int productId = productObject.optInt("id");
                                String productName = productObject.optString("name");
                                int variationId = productObject.optInt("variation_id");
                                int quantity = productObject.optInt("quantity");
                                String  productPrice = String.valueOf(productObject.optInt("price"));
                                String totalPriceOfProduct = productObject.optString("total");

                                OrderProductModel orderProductModel = new OrderProductModel(productId, productName, variationId, quantity, productPrice, totalPriceOfProduct);

                                boolean isAlreadyProductAdded = false;

                                for (OrderProductModel mm: productsList){
                                    if (mm.getId() == productId){
                                        isAlreadyProductAdded = true;
                                    }
                                }

                                if (!isAlreadyProductAdded){
                                    productsList.add(orderProductModel);
                                }
                            }

                            boolean isAlreadyPresent = false;
                            for (OrderModel mm: ordersList){

                                if (mm.getOrderId() == orderId){
                                    isAlreadyPresent = true;
                                }
                            }

                            if (!isAlreadyPresent){
                                ordersList.add(new OrderModel(orderId, orderNumber, orderKey, status, currency, dateCreated, totalOrderPrice, customerId, billingObject, shippingObject, paymentMethod, paymentTitle, trasactionId, datePaid, productsList));
                            }

                        }

                        Log.v("GetAllOrdersData__", "size: " + ordersList.size());

                        if (ordersList.size() != 0){
                            findViewById(R.id.noActiveOrderCard).setVisibility(View.GONE);
                        } else {
                            findViewById(R.id.noActiveOrderCard).setVisibility(View.VISIBLE);
                        }

                        orderAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);

                    } catch (Exception e) {
                        e.printStackTrace();

                        Log.v("GetAllOrdersData__", "Exception: " + e);
                        Toast.makeText(OrdersActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("GetAllOrdersData__", "ErrorListenerError: " + error);
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(OrdersActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

            Volley.newRequestQueue(this).add(jsonStringRequestForOrders);
        }
    }

    AlertDialog waitingDialog;

    private void showLoadingDialog(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(OrdersActivity.this);
        //setting up the layout for alert dialog
        View view1 = LayoutInflater.from(OrdersActivity.this).inflate(R.layout.dialog_please_wait, null, false);

        builder1.setView(view1);

        waitingDialog = builder1.create();
    }

    private final int Pay_Pal_Request_Code = 998;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(Config.PAYPAL_CLIENT_ID);

    public void PayToGet(BigDecimal amount, String Data, OrderModel modell){
        //PayToGet(new BigDecimal(totalAmount), "Order Id : " + id);

        currentProcessingModel = modell;

        PayPalPayment Payment_Call = new PayPalPayment(amount, "USD", Data,
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent Intent_Call = new Intent(OrdersActivity.this, PaymentActivity.class);
        Intent_Call.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        Intent_Call.putExtra(PaymentActivity.EXTRA_PAYMENT, Payment_Call);
        startActivityForResult(Intent_Call, Pay_Pal_Request_Code);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Pay_Pal_Request_Code) {
            if (resultCode == RESULT_OK) {

                PaymentConfirmation Confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (Confirmation != null) {

                    Log.v("Paypal__", "PaypalResponse: " + Confirmation.toJSONObject().toString());

                    try {

                        JSONObject res = Confirmation.toJSONObject();

                        JSONObject responseObject = res.getJSONObject("response");
                        String paymentId = responseObject.optString("id");

                        Toast.makeText(ordersActivity, "Almost Done", Toast.LENGTH_SHORT).show();

                        Log.v("OrderTransactionData__", "PaymentId : " + paymentId);

                        String NEWURL = "https://insignum.hr/wp-json/users/transaction_id?payment_id=" + paymentId;

                        progressDialog.show();
                        ProcessPayment(NEWURL);

                    } catch (Exception e){
                        e.printStackTrace();
                        waitingDialog.dismiss();
                    }

                }
            } else if (resultCode == RESULT_CANCELED) {
                Snackbar.make(findViewById(android.R.id.content), "Payment Canceled!", Snackbar.LENGTH_LONG).show();
                waitingDialog.dismiss();

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Snackbar.make(findViewById(android.R.id.content), "Error Contact CS \nError: ", Snackbar.LENGTH_LONG).show();
                waitingDialog.dismiss();
            }
        }
    }

    private void ProcessPayment(String url){

        Log.v("OrderTransactionData__", "Transaction Details getting started");

        final StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.v("OrderTransactionData__", "Transaction Details Getted");

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("transactions");

                    if (jsonArray.length() != 0){
                        JSONObject transactionObject = jsonArray.getJSONObject(0);

                        JSONArray relatedResources = transactionObject.getJSONArray("related_resources");

                        if (relatedResources.length() != 0){

                            JSONObject beforeSale = relatedResources.getJSONObject(0);

                            JSONObject sale = beforeSale.getJSONObject("sale");

                            String transactionId = sale.optString("id");
                            String datePaid = sale.optString("create_time");

                            Log.v("OrderTransactionData__", "Transaction ID : " + transactionId);

                            nowPushTransactionId(transactionId, datePaid);
                        }
                    }



                } catch (Exception e) {
                    e.printStackTrace();
                    waitingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                waitingDialog.dismiss();

                Log.v("OrderTransactionData__", "getting Error in LIstener: " + error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(OrdersActivity.this).add(request);
    }

    private void nowPushTransactionId(String transactionId, String datePaid){

        Log.v("OrderTransactionData__", "PUshing starting");

        String orderUpdatingUrl = APIsInfo.startURL + "orders/" + currentProcessingModel.getOrderId() + "" + APIsInfo.consumerKeyAndSecretString;

        JSONObject sendingObject = new JSONObject();
        try {
            sendingObject.put("set_paid", true);
            sendingObject.put("transaction_id", transactionId);
            sendingObject.put("date_paid", datePaid);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, orderUpdatingUrl, sendingObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.v("OrderTransactionData__", "Pushing Succesfull");

                if (response.has("id")) {
                    //customerId
                    Snackbar.make(findViewById(android.R.id.content), "Payment Successful!", Snackbar.LENGTH_LONG).show();

                    waitingDialog.dismiss();

                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                waitingDialog.dismiss();
                Log.v("OrderTransactionData__", "PUshing Error in LIstener: " + error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(OrdersActivity.this).add(request);
    }
}
