package jeuxdevelopers.com.insignum.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import jeuxdevelopers.com.insignum.Adapters.CategoryAdapter;
import jeuxdevelopers.com.insignum.Models.CatModelInProducts;
import jeuxdevelopers.com.insignum.Models.CategoryModel;
import jeuxdevelopers.com.insignum.Models.ProductsModel;
import jeuxdevelopers.com.insignum.R;
import jeuxdevelopers.com.insignum.Universal.APIsInfo;

public class MainActivity extends AppCompatActivity {

    ImageView menuIcon;

    public static String GetCustomerData = "";

    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;

    SharedPreferences cart;

    int customerId = 0;

    public static JSONObject billingObject, shippingObject;

    EditText searchEdit;
    RecyclerView catRecycler;
    CategoryAdapter catAdapter;

    private FloatingActionButton iv_Search;

    boolean isStarting = true;

    private Button viewCartBtn;

    private LinearLayout cartHeadingMain;

    List<CategoryModel> categories = new ArrayList<>();
    List<CategoryModel> categoriesTemp = new ArrayList<>();

    public static List<ProductsModel> allProducts = new ArrayList<>();

    //NavigationViews
    DrawerLayout My_Drawer;

    public static enum PaymentMethod {
        DIrectBankTransfer,
        CashOnDelivery,
        Corvus
    }

    //setting and getting main activity
    private static MainActivity mainActivity;

    public static MainActivity getMainActivity() {
        return mainActivity;
    }

    private static void setMainActivity(MainActivity mainActivity) {
        MainActivity.mainActivity = mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivity.setMainActivity(this);
        initViews();

        getCategories();

        getAllProducts();

        Navigation_Work();

        hideSearch();

        iv_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchEdit.getVisibility() == View.VISIBLE) {
                    hideSearch();
                } else {
                    showSearch();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkCartIfEmpty();
    }

    private void initViews() {
        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();
        cart = getSharedPreferences("cart", MODE_PRIVATE);

        customerId = appShared.getInt("customerId", 0);

        catRecycler = findViewById(R.id.catRecycler);
        cartHeadingMain = findViewById(R.id.cartHeadingMain);
        catAdapter = new CategoryAdapter(categories, this);
        catRecycler.setAdapter(catAdapter);
        catRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        catRecycler.setNestedScrollingEnabled(false);

        menuIcon = findViewById(R.id.menuIconMain);
        viewCartBtn = findViewById(R.id.viewCartBtn);
        iv_Search = findViewById(R.id.iv_Search);
        searchEdit = findViewById(R.id.searchEdit);
        searchEdit.setFocusable(true);

        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterName(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        findViewById(R.id.viewCartBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (allProducts.size() == 0) {
                    Toast.makeText(MainActivity.this, "Let The data be fetched first.", Toast.LENGTH_SHORT).show();
                    return;
                }

                startActivity(new Intent(MainActivity.this, CartActivity.class));
            }
        });
    }

    private void filterName(String name) {
        categories.clear();
        catAdapter.notifyDataSetChanged();

        if (name.isEmpty()) {
            categories.addAll(categoriesTemp);
            catAdapter.notifyDataSetChanged();
        } else {
            for (CategoryModel model : categoriesTemp) {
                if (model.getName().toLowerCase().contains(name.toLowerCase())) {
                    categories.add(model);
                    catAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void getCategories() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // Get details on the currently active default data network
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {

            try {
                categories.clear();
                categoriesTemp.clear();

                //progressBar.setVisibility(View.VISIBLE);

                Log.v("GettingData__", "Internet Connected");

                final StringRequest jsonStringRequest = new StringRequest(Request.Method.GET, APIsInfo.GET_CATEGORIES, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            Log.d("Called--- ", "Size----- " + jsonArray.length());
                            Log.v("Called--- ", "Size----- " + jsonArray.length());

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                Log.v("jsonObjectCat__ ", "Object: " + jsonObject.toString());

                                int id = jsonObject.optInt("id");
                                String name = jsonObject.optString("name");
                                String slug = jsonObject.optString("slug");
                                int parentId = jsonObject.optInt("parent");

                                String imagelink = "";

                                if (jsonObject.optJSONObject("image") != null) {
                                    JSONObject imageObject = jsonObject.getJSONObject("image");

                                    imagelink = imageObject.optString("src");
                                } else {
                                    imagelink = "https://insignum.hr/wp-content/uploads/2019/04/oprema-1.jpg";
                                }

                                Log.v("Parent__", "ParentName; " + parentId);

                                String description = jsonObject.optString("description");
                                String display = jsonObject.optString("display");
                                int menu_order = jsonObject.optInt("menu_order");
                                int count = jsonObject.optInt("count");

//                                if (id == 99) {
//                                    Log.v("GettingData__", "Women Category");
//                                }

                                CategoryModel model = new CategoryModel(id, name, imagelink, slug, parentId, description, display, menu_order, count);

                                Log.v("Parent__", "CatModel; " + model);

                                boolean isAlreadyPresent = false;

                                for (CategoryModel mm : categories) {
                                    if (mm.getId() == id) {
                                        isAlreadyPresent = true;
                                    }
                                }

                                if (!isAlreadyPresent) {

                                    if (model.getCount() != 0) {
                                        categories.add(model);
                                        categoriesTemp.add(model);

                                    }


                                    Log.v("GettingData__", "Name: " + model.getName() + " Count" + model.getCount());


                                }
                            }

                            Log.v("GettingData__", "Size: " + categories.size());

                            showDataNow();

                        } catch (Exception e) {
                            e.printStackTrace();

                            getCategories();

                            Log.v("GettingData__", "Error here: " + e);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("GettingData__", "ErrorListenerError: " + error);
                        getCategories();
                    }
                });

                jsonStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        30000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                Volley.newRequestQueue(MainActivity.this).add(jsonStringRequest);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showDataNow() {
        findViewById(R.id.avi).setVisibility(View.GONE);
        catAdapter.notifyDataSetChanged();
    }

    private void getAllProducts() {

        Log.v("GettingAllProducts__", "Started");

        allProducts.clear();

        try {
            for (int i = 1; i < 30; i++) {
                final StringRequest jsonStringRequestForProducts = new StringRequest(com.android.volley.Request.Method.GET, APIsInfo.GET_ALL_PRODUCTS + i, new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                Log.v("GettingAllProducts__", "ProductObject: " + jsonObject);

                                int id = jsonObject.optInt("id");
                                String name = jsonObject.optString("name");
                                String sku = jsonObject.optString("sku");
                                String slug = jsonObject.optString("slug");
                                String permalink = jsonObject.optString("permalink");
                                String type = jsonObject.optString("type");
                                String status = jsonObject.optString("status");

                                List<Integer> relatedIds = new ArrayList<>();

                                if (jsonObject.has("related_ids")) {

                                    JSONArray relatedArrayJson = jsonObject.getJSONArray("related_ids");

                                    for (int relatedId = 0; relatedId < relatedArrayJson.length(); relatedId++) {

                                        int relatedProductId = relatedArrayJson.optInt(relatedId);
                                        relatedIds.add(relatedProductId);
                                    }
                                }

                                if (status.equals("publish")) {

                                    boolean manageStock = jsonObject.optBoolean("manage_stock");
                                    String stockStatus = jsonObject.optString("stock_status");
                                    int stockQuantity = jsonObject.optInt("stock_quantity");

                                    boolean featured = jsonObject.optBoolean("featured");
                                    String catalog_visibility = jsonObject.optString("catalog_visibility");
                                    String description = jsonObject.optString("description");
                                    String short_description = jsonObject.optString("short_description");
                                    String price = jsonObject.optString("price");
                                    Log.v("PriceOriginal", price);
                                    if (TextUtils.isEmpty(price)) {
                                        price = "0";
                                    }
                                    Log.v("PriceUpdated", price);
                                    String regular_price = jsonObject.optString("regular_price");
                                    String date_on_sale_from = jsonObject.optString("date_on_sale_from");
                                    String date_on_sale_to = jsonObject.optString("date_on_sale_to");
                                    boolean on_sale = jsonObject.optBoolean("on_sale");
                                    boolean purchasable = jsonObject.optBoolean("purchasable");
                                    int total_sales = jsonObject.optInt("total_sales");
                                    int parent_id = jsonObject.optInt("parent_id");
                                    String purchase_note = jsonObject.getString("purchase_note");

                                    List<CatModelInProducts> catIds = new ArrayList<>();
                                    List<String> imageUrls = new ArrayList<>();
                                    List<String> tagList = new ArrayList<>();

                                    JSONArray categoryArray = jsonObject.getJSONArray("categories");
                                    for (int j = 0; j < categoryArray.length(); j++) {
                                        JSONObject catObject = categoryArray.getJSONObject(j);
                                        int catId = catObject.optInt("id");
                                        String catName = catObject.optString("name");
                                        catIds.add(new CatModelInProducts(catId, catName));
                                    }

                                    JSONArray imageArray = jsonObject.getJSONArray("images");
                                    for (int j = 0; j < imageArray.length(); j++) {
                                        JSONObject imageObject = imageArray.getJSONObject(j);
                                        String imageUrl = imageObject.optString("src");
                                        imageUrls.add(imageUrl);
                                    }

                                    JSONArray tagArray = jsonObject.getJSONArray("tags");
                                    for (int j = 0; j < tagArray.length(); j++) {
                                        JSONObject tagObject = tagArray.getJSONObject(j);
                                        String tag = tagObject.optString("name");
                                        tagList.add(tag);
                                    }

                                    ProductsModel model = new ProductsModel(id, name, slug, permalink, type, status, featured, catalog_visibility, description, short_description, price, regular_price, date_on_sale_from, date_on_sale_to, on_sale, purchasable, total_sales, parent_id, purchase_note, catIds, imageUrls, tagList, manageStock, stockStatus, stockQuantity, sku, relatedIds);

                                    boolean isAlreadyPresent = false;

                                    for (ProductsModel mm : allProducts) {
                                        if (mm.getId() == id) {
                                            isAlreadyPresent = true;
                                        }
                                    }

                                    if (!isAlreadyPresent) {
                                        allProducts.add(model);
                                    }
                                }
                            }

                            Log.v("GettingAllProducts__", "Size: " + allProducts.size());

                            Log.v("GettingAllProducts__", "getted");

                            if (isStarting) {

                                Log.v("GettingAllProducts__", "Main Fragment Loaded");

                                isStarting = false;
//                            mainProgress.setVisibility(View.GONE);
//                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_main, new MainFragment()).commit();
                            } else {
                                Log.v("GettingAllProducts__", "Main Fragment already loaded");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Log.v("GettingAllProducts__", "Error here: " + e);
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (isStarting) {
                            getAllProducts();
                        }
                        Log.v("GettingAllProducts__", "ErrorListenerError: " + error);
                    }
                });

                jsonStringRequestForProducts.setRetryPolicy(new DefaultRetryPolicy(
                        30000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                Volley.newRequestQueue(this).add(jsonStringRequestForProducts);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        customerId = appShared.getInt("customerId", 0);

        if (customerId != 0) {

            GetCustomerData = "https://insignum.hr/wp-json/wc/v3/customers/" + customerId + APIsInfo.consumerKeyAndSecretString;

            getCustomersData();
        } else {
            //startActivity(new Intent(MainActivity.this, LoginActivity.class));
            //finish();
        }
    }

    private void getCustomersData() {
        Log.v("GettingCUstomerData__", "Started");

        if (billingObject == null || shippingObject == null) {

            Log.v("GettingCUstomerData__", "null");
            //Snackbar.make(findViewById(android.R.id.content), "Getting Data", Snackbar.LENGTH_SHORT).show();

//            Snackbar snackbar = Snackbar.make(v, "Getting Data", Snackbar.LENGTH_SHORT);
//            View snackbarView = snackbar.getView();
//            snackbarView.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));

            billingObject = new JSONObject();
            shippingObject = new JSONObject();

            final StringRequest jsonStringRequest = new StringRequest(Request.Method.GET, GetCustomerData, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        Log.v("GettingCUstomerData__", "Customer: " + jsonResponse.toString());

                        if (jsonResponse.has("id")) {

                            Snackbar.make(findViewById(android.R.id.content), "Welcome Back " + jsonResponse.optString("username"), Snackbar.LENGTH_SHORT).show();

                            billingObject = jsonResponse.getJSONObject("billing");
                            shippingObject = jsonResponse.getJSONObject("shipping");

                            Log.v("GettingCUstomerData__", "Data IS Here: " + billingObject + "\n\n" + shippingObject);

                        } else if (jsonResponse.has("message")) {
                            Log.v("GettingCUstomerData__", "Error: " + jsonResponse);
                            Toast.makeText(MainActivity.this, "Error: " + jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.v("GettingCUstomerData__", "Error e: " + e);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("GettingCUstomerData__", "ErrorListenerError: " + error);
                }
            });

            jsonStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            Volley.newRequestQueue(MainActivity.this).add(jsonStringRequest);

        } else {
            Log.v("GettingCUstomerData__", "Data IS Here: " + billingObject + "\n\n" + shippingObject);
        }
    }

    private void Navigation_Work() {
        NavigationView navigation_View = findViewById(R.id.nav_view);
        My_Drawer = findViewById(R.id.drawerLayout);

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //      Toast.makeText(MainActivity.this, "Here", Toast.LENGTH_SHORT).show();
                if (My_Drawer.isDrawerOpen(Gravity.RIGHT)) {
                    My_Drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    My_Drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });

        TextView orderText = navigation_View.findViewById(R.id.ordersText);

        TextView home = navigation_View.findViewById(R.id.home);
        home.setOnClickListener(v -> My_Drawer.closeDrawer(Gravity.RIGHT));

        orderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, OrdersActivity.class));

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });
        TextView profileText = navigation_View.findViewById(R.id.profileText);
        profileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (customerId == 0) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                } else {
                    startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                }
//                if (FirebaseAuth.getInstance().getCurrentUser() == null){
//                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                    finish();
//                } else {
//                    startActivity(new Intent(MainActivity.this, ProfileActivity.class));
//                }

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        TextView about = navigation_View.findViewById(R.id.about);
        TextView contactUsText = navigation_View.findViewById(R.id.contactUsText);
        TextView about_us = navigation_View.findViewById(R.id.about_us);
        TextView privacyStatement = navigation_View.findViewById(R.id.privacyStatement);

        privacyStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                My_Drawer.closeDrawer(Gravity.RIGHT);
                startActivity(new Intent(MainActivity.this, PrivacyStatementActivity.class));
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contactUsText.getVisibility() == View.VISIBLE) {
                    hideView(contactUsText);
                    hideView(about_us);
                } else {
                    showView(contactUsText);
                    showView(about_us);
                }
            }
        });

        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        contactUsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, ContactUsActivity.class));

//                if (FirebaseAuth.getInstance().getCurrentUser() == null){
//                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                    finish();
//                } else {
//                    startActivity(new Intent(MainActivity.this, ChatWithAdminActivity.class));
//                }

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        TextView checkStatusNav = navigation_View.findViewById(R.id.checkStatusNav);
        checkStatusNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, OrderStatusCheckActivity.class));
            }
        });

        TextView trackStore = navigation_View.findViewById(R.id.textViewTrackShops);
        trackStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ShowStoreLocationActivity.class));
            }
        });

        TextView logoutText = navigation_View.findViewById(R.id.logoutText);
        TextView webShop = navigation_View.findViewById(R.id.webShop);
        TextView profile = navigation_View.findViewById(R.id.profile);

        if (customerId == 0) {
            logoutText.setText("Login");
        } else {
            logoutText.setText("Logout");
        }

        webShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderText.getVisibility() == View.VISIBLE) {
                    hideView(orderText);
                } else {
                    showView(orderText);
                }
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profileText.getVisibility() == View.VISIBLE) {
                    hideView(profileText);
                    hideView(logoutText);
                } else {
                    showView(profileText);
                    showView(logoutText);
                }
            }
        });

        logoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (customerId == 0) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                } else {
                    appSharedEditor.putInt("customerId", 0);
                    appSharedEditor.apply();

                    startActivity(new Intent(MainActivity.this, WelcomeScreen.class));
                    finish();

                    try {
                        My_Drawer.closeDrawer(Gravity.RIGHT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void showSnackbar(String message) {

        if (message.isEmpty()) {
            message = "Message";
        }

        Toast.makeText(mainActivity, "" + message, Toast.LENGTH_LONG).show();
    }

    private void hideSearch() {
        searchEdit.setText("");
        searchEdit.setVisibility(View.GONE);
        iv_Search.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_search));
    }

    private void showSearch() {
        searchEdit.setVisibility(View.VISIBLE);
        iv_Search.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_close));
    }

    private void checkCartIfEmpty() {
        int numberOfCartItemss = cart.getInt("NoOfCartItems", 0);
        if (numberOfCartItemss <= 0) {
            viewCartBtn.setVisibility(View.GONE);
            cartHeadingMain.setVisibility(View.GONE);
        } else {
            viewCartBtn.setVisibility(View.VISIBLE);
            cartHeadingMain.setVisibility(View.VISIBLE);
        }
    }

    private void hideView(TextView view) {
        view.setVisibility(View.GONE);
    }

    private void showView(TextView view) {
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
