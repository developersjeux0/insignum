package jeuxdevelopers.com.insignum.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import jeuxdevelopers.com.insignum.R;

public class OrderStatusCheckActivity extends AppCompatActivity {

    EditText orderEdit, codeEdit;
    Button checkBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status_check);

        orderEdit = findViewById(R.id.orderEdit);
        codeEdit = findViewById(R.id.codeEdit);

        checkBtn = findViewById(R.id.checkBtn);
        showLoadingDialog();

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        checkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orderEdit.getText().toString().isEmpty() && codeEdit.getText().toString().isEmpty()){
                    Toast.makeText(OrderStatusCheckActivity.this, "Input the order number and code number", Toast.LENGTH_SHORT).show();
                    return;
                }

                sendRequest();
            }
        });
    }

    String messageStatus = "";

    private void sendRequest(){

        waitingDialog.show();

        String URL = "https://insignum.hr/wp-json/users/order_track?order_number=" + orderEdit.getText().toString() + "&code=" + codeEdit.getText().toString();

        final StringRequest jsonStringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                messageStatus = response;

              //  waitingDialog.show();
                statusMessage.setText(messageStatus);
                progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                waitingDialog.dismiss();
                Toast.makeText(OrderStatusCheckActivity.this, "Error "+error, Toast.LENGTH_SHORT).show();

                Log.v("GettingCUstomerData__", "ErrorListenerError: " + error);
            }
        });

        jsonStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(OrderStatusCheckActivity.this).add(jsonStringRequest);
    }

    AlertDialog waitingDialog;
    TextView statusMessage;
    ProgressBar progressBar;

    private void showLoadingDialog()

    {


        AlertDialog.Builder builder1 = new AlertDialog.Builder(OrderStatusCheckActivity.this);
        //setting up the layout for alert dialog
        View view1 = LayoutInflater.from(OrderStatusCheckActivity.this).inflate(R.layout.dialog_please_wait, null, false);
        builder1.setView(view1);

             statusMessage = view1.findViewById(R.id.messageText);
             progressBar = view1.findViewById(R.id.progressBar);

        waitingDialog = builder1.create();
        waitingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }
}
