package jeuxdevelopers.com.insignum.Activities;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.reginald.editspinner.EditSpinner;

import org.json.JSONObject;

import jeuxdevelopers.com.insignum.R;
import jeuxdevelopers.com.insignum.Universal.APIsInfo;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GetBillingDataActivity extends AppCompatActivity {

    private EditText editText_Address1, editText_City, editText_State, editText_PostCode;
    private String firstName, lastName, phone, strAddress1, strCity, strState = "", strPostCode, strCountry, customerId;
    EditSpinner countryEditSpinner;
    Button btn_Save;
    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;
    private ProgressBar progressBar;
    JSONObject billingObject, shippingObject;
    private AlertDialog waitingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_billing_data);
        initViews();
        getCurrentCustomerData();

        findViewById(R.id.btn_Save).setOnClickListener(v -> {
            getData();
            if (TextUtils.isEmpty(strAddress1) || TextUtils.isEmpty(strCity) || TextUtils.isEmpty(strPostCode) || TextUtils.isEmpty(strCountry)) {
                Toast.makeText(GetBillingDataActivity.this, "Please Fill in All the fields first.", Toast.LENGTH_SHORT).show();
                return;
            }

            btn_Save.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            sendRequestNow();
        });

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initViews() {

        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        editText_Address1 = findViewById(R.id.addressEdit);
        editText_City = findViewById(R.id.cityEdit);
        editText_State = findViewById(R.id.stateEdit);
        editText_PostCode = findViewById(R.id.postcodeEdit);
        countryEditSpinner = findViewById(R.id.countryEditSpinner);
        btn_Save = findViewById(R.id.btn_Save);
        progressBar = findViewById(R.id.progressBar);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.countries_array));

        countryEditSpinner.setAdapter(adapter);
    }

    private void getData() {
        strAddress1 = editText_Address1.getText().toString();
        strCity = editText_City.getText().toString();
        strState = editText_State.getText().toString();
        strPostCode = editText_PostCode.getText().toString();
        strCountry = countryEditSpinner.getText().toString();
    }

    private void getCurrentCustomerData() {

        showLoadingDialog("Getting you Information...");

        final int customerId = appShared.getInt("customerId", 0);

        String GETTINGCUSTOMERDATAURL = "https://afrogarm.co.uk/wp-json/wc/v3/customers/" + customerId + "?consumer_key=ck_112b54602019062f1b6d063d452fa05e52814255&consumer_secret=cs_822aadf5830f569b2c0697308fd0c9704e9e964e";


        GETTINGCUSTOMERDATAURL = "https://insignum.hr/wp-json/wc/v3/customers/" + customerId + APIsInfo.consumerKeyAndSecretString;

        final StringRequest jsonStringRequest = new StringRequest(com.android.volley.Request.Method.GET, GETTINGCUSTOMERDATAURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    if (jsonResponse.has("id")) {

                        firstName = jsonResponse.optString("first_name");
                        lastName = jsonResponse.optString("last_name");
                        billingObject = jsonResponse.getJSONObject("billing");
                        phone = billingObject.optString("phone");

                        waitingDialog.dismiss();

                    } else if (jsonResponse.has("message")) {
                        waitingDialog.dismiss();
                        Toast.makeText(GetBillingDataActivity.this, "Error: " + jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                        Log.v("GettingCUstomerData__", "Error: " + jsonResponse);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(GetBillingDataActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.v("GettingCUstomerData__", "Error e: " + e);
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("GettingCUstomerData__", "ErrorListenerError: " + error);
                Toast.makeText(GetBillingDataActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        Volley.newRequestQueue(GetBillingDataActivity.this).add(jsonStringRequest);
    }

    private void showLoadingDialog(String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(GetBillingDataActivity.this);
        //setting up the layout for alert dialog
        View view1 = LayoutInflater.from(GetBillingDataActivity.this).inflate(R.layout.dialog_please_wait, null, false);

        builder1.setView(view1);

        TextView messageText = view1.findViewById(R.id.messageText);
        messageText.setText(message);

        waitingDialog = builder1.create();

        waitingDialog.show();
    }

    private void sendRequestNow() {

        final String url = "https://insignum.hr/wp-json/users/updateCustomer";

        final int customerId = appShared.getInt("customerId", 0);

        if (customerId == 0) {
            Snackbar.make(findViewById(android.R.id.content), "You are not logged in right now.", Snackbar.LENGTH_LONG).show();
            return;
        }

        //OKHTTP
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String bodyy = String.format("{\n\"id\":\"%s\",\n\"first_name\":\"%s\",\"last_name\":\"%s\",\"billing_country\":\"%s\",\"billing\":{\"first_name\":\"%s\",\"last_name\":\"%s\",\"company\":\"%s\",\"address_1\":\"%s\",\"address_2\":\"\",\"city\":\"%s\",\"state\":\"%s\",\"postcode\":\"%s\",\"country\":\"%s\",\"phone\":\"%s\"},\"shipping\":{\"first_name\":\"%s\",\"last_name\":\"%s\",\"company\":\"%s\",\"address_1\":\"%s\",\"address_2\":\"\",\"city\":\"%s\",\"state\":\"%s\",\"postcode\":\"%s\",\"country\":\"%s\"}}",
                            String.valueOf(appShared.getInt("customerId", 0)), firstName, lastName, strCountry,
                            firstName, lastName, "", strAddress1, strCity, "", strPostCode, strCountry, phone,
                            firstName, lastName, "", strAddress1, strCity, "", strPostCode, strCountry);


                    OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody body = RequestBody.create(mediaType, bodyy);
                    Request request = new Request.Builder()
                            .url(url)
                            .post(body)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("User-Agent", "PostmanRuntime/7.20.1")
                            .addHeader("Accept", "*/*")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Postman-Token", "727d7fff-0556-4d19-a64d-bf1aa8490588,a8d76b22-94b5-4154-bd13-a217aff279b9")
                            .addHeader("Host", "insignum.hr")
                            //.addHeader("Accept-Encoding", "gzip, deflate")
                            .addHeader("Content-Length", "591")
                            .addHeader("Connection", "keep-alive")
                            .addHeader("cache-control", "no-cache")
                            //.build();
                            .build();

                    final Response response = client.newCall(request).execute();

                    String ress = response.body().string();

                    Log.v("RegistrationProgress__", "Message: " + ress);

                    if (!ress.isEmpty()) {
                        final JSONObject responseObject = new JSONObject(ress);

                        if (responseObject.has("id")) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(GetBillingDataActivity.this, "Billing Information Saved", Toast.LENGTH_SHORT).show();
                                    int id = responseObject.optInt("id");

                                    waitingDialog.dismiss();

                                    appSharedEditor.putInt("customerId", id);
                                    appSharedEditor.putBoolean("isBillingDataAvailable", true);
                                    appSharedEditor.apply();

                                    Log.v("RegistrationProgress__", "ID Registered: " + id);

                                    finish();
                                }
                            });

                        } else if (responseObject.has("message")) {
                            final String errorMessage = responseObject.optString("message");

                            btn_Save.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(GetBillingDataActivity.this, "Error: " + errorMessage, Toast.LENGTH_SHORT).show();
                                    waitingDialog.dismiss();

                                    btn_Save.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    Log.v("RegistrationProgress__", "ResponseOKHTTP: " + response.isSuccessful());
                                    Log.v("RegistrationProgress__", "ResponseOKHTTPCode: " + response.code());

                                    waitingDialog.dismiss();
                                }
                            });
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    btn_Save.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }
            }
        }).start();
    }
}
