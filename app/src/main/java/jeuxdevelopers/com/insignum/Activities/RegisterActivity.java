package jeuxdevelopers.com.insignum.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.reginald.editspinner.EditSpinner;

import org.json.JSONObject;

import jeuxdevelopers.com.insignum.R;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity {

    private EditText editText_Email, editText_FirstName, editText_LastName, editText_Username, editText_Password, editText_ConfirmPassword, editText_Phone,
            editText_Company, editText_Address1, editText_City, editText_State, editText_PostCode;

    private String strEmail, strFirstName, strLastName, strUsername, strPassword, strConfirmPassword, strPhone,
            strCompany, strAddress1, strCity, strState, strPostCode, strCountry;

    EditSpinner countryEditSpinner;

    Button signupBtn;

    String WEBSITE_URL = "https://insignum.hr/wp-json/wc/v3/";
    String REGISTERATION_URL = "customers?consumer_key=ck_55aa3f4a157322610059faf4beb4f61f3b08cc64&consumer_secret=cs_3e5dbc594f5543838dd6ed0dd96f41fa7ff89d0d";

    String REGISTRATIONLINK = "https://insignum.hr/wp-json/users/register?consumer_key=ck_55aa3f4a157322610059faf4beb4f61f3b08cc64&consumer_secret=cs_3e5dbc594f5543838dd6ed0dd96f41fa7ff89d0d";

    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;

    CheckBox businessCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();

                //Previous Code in which billing was mandatory
//                if (TextUtils.isEmpty(strEmail) || TextUtils.isEmpty(strFirstName) || TextUtils.isEmpty(strLastName) || TextUtils.isEmpty(strUsername) || TextUtils.isEmpty(strPassword) || TextUtils.isEmpty(strConfirmPassword) || TextUtils.isEmpty(strPhone) || TextUtils.isEmpty(strAddress1) || TextUtils.isEmpty(strCity) || TextUtils.isEmpty(strState) || TextUtils.isEmpty(strPostCode) || TextUtils.isEmpty(strCountry)) {
//                    Toast.makeText(RegisterActivity.this, "Please Fill in All the fields first.", Toast.LENGTH_SHORT).show();
//                    return;
//                }

                //New Code, here billing is not mandatory
                if (TextUtils.isEmpty(strEmail) || TextUtils.isEmpty(strFirstName) || TextUtils.isEmpty(strLastName) || TextUtils.isEmpty(strUsername) || TextUtils.isEmpty(strPassword) || TextUtils.isEmpty(strConfirmPassword) || TextUtils.isEmpty(strPhone)) {
                    Toast.makeText(RegisterActivity.this, "Please Fill in All the fields first.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!strPassword.equals(strConfirmPassword)) {
                    editText_Password.setError("Your Passwords doesn't match.");
                    return;
                }

                registerUser();
            }
        });
    }

    private void getData() {
        strEmail = editText_Email.getText().toString();
        strFirstName = editText_FirstName.getText().toString();
        strLastName = editText_LastName.getText().toString();
        strUsername = editText_Username.getText().toString();
        strPassword = editText_Password.getText().toString();
        strConfirmPassword = editText_ConfirmPassword.getText().toString();
        strPhone = editText_Phone.getText().toString();
        strCompany = editText_Company.getText().toString();
        strAddress1 = editText_Address1.getText().toString();
        strCity = editText_City.getText().toString();
        strState = editText_State.getText().toString();
        strPostCode = editText_PostCode.getText().toString();
        strCountry = countryEditSpinner.getText().toString();
    }

    AlertDialog waitingDialog;

    private void showLoadingDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(RegisterActivity.this);
        //setting up the layout for alert dialog
        View view1 = LayoutInflater.from(RegisterActivity.this).inflate(R.layout.dialog_please_wait, null, false);

        builder1.setView(view1);

        waitingDialog = builder1.create();
    }

    private void initViews() {

        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        //EditTextFields
        showLoadingDialog();
        businessCheck = findViewById(R.id.businessUserCheck);

        businessCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    editText_Company.setVisibility(View.VISIBLE);
                } else {
                    editText_Company.setVisibility(View.GONE);
                }
            }
        });

        editText_Email = findViewById(R.id.emailEdit);
        editText_FirstName = findViewById(R.id.firstnameEdit);
        editText_LastName = findViewById(R.id.lastnameEdit);
        editText_Username = findViewById(R.id.usernameEdit);
        editText_Password = findViewById(R.id.passwordEdit);
        editText_ConfirmPassword = findViewById(R.id.confirmPasswordEdit);
        editText_Phone = findViewById(R.id.phoneNumberEdit);
        editText_Company = findViewById(R.id.companyEdit);
        editText_Address1 = findViewById(R.id.addressEdit);
        editText_City = findViewById(R.id.cityEdit);
        editText_State = findViewById(R.id.stateEdit);
        editText_PostCode = findViewById(R.id.postcodeEdit);
        countryEditSpinner = findViewById(R.id.countryEditSpinner);
        signupBtn = findViewById(R.id.registerBtn);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.countries_array));

        countryEditSpinner.setAdapter(adapter);
    }

    private void registerUser() {
        waitingDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    OkHttpClient client = new OkHttpClient();

                    String bodyy = String.format("" +
                                    "{\n\"email\":\"%s\",\n\"first_name\":\"%s\",\"last_name\":\"%s\",\"username\":\"%s\",\"password\":\"%s\",\"password2\":\"%s\"," +
                                    "\"billing\":{\"first_name\":\"%s\",\"last_name\":\"%s\",\"company\":\"%s\",\"address_1\":\"%s\",\"address_2\":\"%s\",\"city\":\"%s\",\"state\":\"%s\",\"postcode\":\"%s\",\"country\":\"%s\",\"email\":\"%s\",\"phone\":\"%s\"}," +
                                    "\"shipping\":{\"first_name\":\"%s\",\"last_name\":\"%s\",\"company\":\"%s\",\"address_1\":\"%s\",\"address_2\":\"%s\",\"city\":\"%s\",\"state\":\"%s\",\"postcode\":\"%s\",\"country\":\"%s\"}}",
                            strEmail, strFirstName, strLastName, strUsername, strPassword, strPassword,
                            strFirstName, strLastName, strCompany, strAddress1, "", strCity, strState, strPostCode, strCountry, strEmail, strPhone,
                            strFirstName, strLastName, strCompany, strAddress1, "", strCity, strState, strPostCode, strCountry);

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody body = RequestBody.create(mediaType, bodyy);
                    Request request = new Request.Builder()
                            .url(REGISTRATIONLINK)
                            .post(body)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("User-Agent", "PostmanRuntime/7.20.1")
                            .addHeader("Accept", "*/*")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Postman-Token", "727d7fff-0556-4d19-a64d-bf1aa8490588,a8d76b22-94b5-4154-bd13-a217aff279b9")
                            .addHeader("Host", "insignum.hr")
                            //.addHeader("Accept-Encoding", "gzip, deflate")
                            .addHeader("Content-Length", "591")
                            .addHeader("Connection", "keep-alive")
                            .addHeader("cache-control", "no-cache")
                            .build();

                    final Response response = client.newCall(request).execute();

                    String ress = response.body().string();

                    if (!ress.isEmpty()) {
                        Log.v("RegistrationResponse", "Response not Empty");
                        final JSONObject responseObject = new JSONObject(ress);

                        if (responseObject.has("id")) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(RegisterActivity.this, "Registration Successful.", Toast.LENGTH_SHORT).show();
                                    int id = responseObject.optInt("id");

                                    waitingDialog.dismiss();

                                    appSharedEditor.putInt("customerId", id);
                                    appSharedEditor.putBoolean("isBillingDataAvailable", false);
                                    appSharedEditor.apply();

                                    Log.v("RegistrationProgress__", "ID Registered: " + id);

                                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                    try {
                                        LoginActivity.getLoginActivity().finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    finish();
                                }
                            });

                        } else if (responseObject.has("message")) {
                            final String errorMessage = responseObject.optString("message");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(RegisterActivity.this, "Error: " + errorMessage, Toast.LENGTH_SHORT).show();
                                    waitingDialog.dismiss();

                                    Log.v("RegistrationProgress__", "ResponseOKHTTP: " + response.isSuccessful());
                                    Log.v("RegistrationProgress__", "ResponseOKHTTPCode: " + response.code());

                                    waitingDialog.dismiss();
                                }
                            });
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }
}
