package jeuxdevelopers.com.insignum.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import jeuxdevelopers.com.insignum.R;

public class PrivacyStatementActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout lnr_1, lnr_Detail1, lnr_2, lnr_Detail2, lnr_3, lnr_Detail3, lnr_4, lnr_Detail4, lnr_5, lnr_Detail5;
    private ArrayList<View> lnrDetailIds, imgIds;
    private ImageView img1, img2, img3, img4, img5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_statement);

        linkViews();
        setClickListeners();
        init();

        findViewById(R.id.backBtn).setOnClickListener(v -> finish());
    }

    private void init() {

        imgIds = new ArrayList<>();
        imgIds.add(img1);
        imgIds.add(img2);
        imgIds.add(img3);
        imgIds.add(img4);
        imgIds.add(img5);

        lnrDetailIds = new ArrayList<>();
        lnrDetailIds.add(lnr_Detail1);
        lnrDetailIds.add(lnr_Detail2);
        lnrDetailIds.add(lnr_Detail3);
        lnrDetailIds.add(lnr_Detail4);
        lnrDetailIds.add(lnr_Detail5);
    }

    private void linkViews() {
        lnr_1 = findViewById(R.id.lnr_1);
        lnr_2 = findViewById(R.id.lnr_2);
        lnr_3 = findViewById(R.id.lnr_3);
        lnr_4 = findViewById(R.id.lnr_4);
        lnr_5 = findViewById(R.id.lnr_5);
        lnr_Detail1 = findViewById(R.id.lnr_Detail1);
        lnr_Detail2 = findViewById(R.id.lnr_Detail2);
        lnr_Detail3 = findViewById(R.id.lnr_Detail3);
        lnr_Detail4 = findViewById(R.id.lnr_Detail4);
        lnr_Detail5 = findViewById(R.id.lnr_Detail5);
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);
        img5 = findViewById(R.id.img5);
    }

    private void setClickListeners() {
        lnr_1.setOnClickListener(this);
        lnr_2.setOnClickListener(this);
        lnr_3.setOnClickListener(this);
        lnr_4.setOnClickListener(this);
        lnr_5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnr_1:
                if (checkVisibility(lnr_Detail1)) {
                    hideVisibility(img1, lnr_Detail1);
                } else {
                    showAndHide(lnr_Detail1);
                }
                break;
            case R.id.lnr_2:
                if (checkVisibility(lnr_Detail2)) {
                    hideVisibility(img2, lnr_Detail2);
                } else {
                    showAndHide(lnr_Detail2);
                }
                break;
            case R.id.lnr_3:
                if (checkVisibility(lnr_Detail3)) {
                    hideVisibility(img3, lnr_Detail3);
                } else {
                    showAndHide(lnr_Detail3);
                }
                break;
            case R.id.lnr_4:
                if (checkVisibility(lnr_Detail4)) {
                    hideVisibility(img4, lnr_Detail4);
                } else {
                    showAndHide(lnr_Detail4);
                }
                break;
            case R.id.lnr_5:
                if (checkVisibility(lnr_Detail5)) {
                    hideVisibility(img5, lnr_Detail5);
                } else {
                    showAndHide(lnr_Detail5);
                }
                break;
        }
    }

    private void showAndHide(View lnrDetailId) {
        for (int i = 0; i < lnrDetailIds.size(); i++) {
            if (lnrDetailIds.get(i).equals(lnrDetailId)) {
                lnrDetailIds.get(i).setVisibility(View.VISIBLE);
                imgIds.get(i).setBackgroundResource(R.drawable.ic_remove_black_24dp);
            } else {
                lnrDetailIds.get(i).setVisibility(View.GONE);
                imgIds.get(i).setBackgroundResource(R.drawable.ic_add);
            }
        }
    }

    private boolean checkVisibility(View view) {
        return view.getVisibility() == View.VISIBLE;
    }

    private void hideVisibility(View img, View lnr) {
        lnr.setVisibility(View.GONE);
        img.setBackgroundResource(R.drawable.ic_add);
    }
}
