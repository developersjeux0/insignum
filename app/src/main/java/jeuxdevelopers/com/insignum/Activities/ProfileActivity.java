package jeuxdevelopers.com.insignum.Activities;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import jeuxdevelopers.com.insignum.R;
import jeuxdevelopers.com.insignum.Universal.APIsInfo;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileActivity extends AppCompatActivity {

    EditText phoneEdit, firstNameEdit, lastNameEdit, companyEditBilling, addressEditBilling, cityEditBilling, stateEditBilling, postCodeEditBilling, countryEditBilling, companyEditShipping, addressEditShipping, cityEditShipping, stateEditShipping, postCodeEditShipping, countryEditShipping;
    Button updateProfileBtn;

    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;

    JSONObject billingObject, shippingObject;

    private void getCurrentCustomerData() {

        waitingDialog.show();

        final int customerId = appShared.getInt("customerId", 0);

        String GETTINGCUSTOMERDATAURL = "https://afrogarm.co.uk/wp-json/wc/v3/customers/" + customerId + "?consumer_key=ck_112b54602019062f1b6d063d452fa05e52814255&consumer_secret=cs_822aadf5830f569b2c0697308fd0c9704e9e964e";

        Snackbar.make(findViewById(android.R.id.content), "Getting Data", Snackbar.LENGTH_LONG).show();
        billingObject = new JSONObject();
        shippingObject = new JSONObject();

        GETTINGCUSTOMERDATAURL = "https://insignum.hr/wp-json/wc/v3/customers/" + customerId + APIsInfo.consumerKeyAndSecretString;

        final StringRequest jsonStringRequest = new StringRequest(com.android.volley.Request.Method.GET, GETTINGCUSTOMERDATAURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    if (jsonResponse.has("id")) {

                        Snackbar.make(findViewById(android.R.id.content), "Welcome " + jsonResponse.optString("username"), Snackbar.LENGTH_LONG).show();

                        Log.v("asfjbdsad", jsonResponse.toString());

                        billingObject = jsonResponse.getJSONObject("billing");
                        shippingObject = jsonResponse.getJSONObject("shipping");

                        String phone = billingObject.optString("phone");
                        phoneEdit.setText(phone);

                        String firstName = jsonResponse.optString("first_name");
                        firstNameEdit.setText(firstName);

                        lastNameEdit.setText(jsonResponse.optString("last_name"));

                        companyEditBilling.setText(billingObject.optString("company"));
                        companyEditShipping.setText(shippingObject.optString("company"));

                        addressEditBilling.setText(billingObject.optString("address_1"));
                        addressEditShipping.setText(shippingObject.optString("address_1"));

                        cityEditBilling.setText(billingObject.optString("city"));
                        cityEditShipping.setText(shippingObject.optString("city"));

                        postCodeEditBilling.setText(billingObject.optString("postcode"));
                        postCodeEditShipping.setText(shippingObject.optString("postcode"));

                        stateEditBilling.setText(billingObject.optString("state"));
                        stateEditShipping.setText(shippingObject.optString("state"));

                        countryEditBilling.setText(billingObject.optString("country"));
                        countryEditShipping.setText(shippingObject.optString("country"));

                        waitingDialog.dismiss();

                        Log.v("GettingCUstomerData__", "Data IS Here: " + billingObject + "\n\n" + shippingObject);

                    } else if (jsonResponse.has("message")) {
                        Log.v("GettingCUstomerData__", "Error: " + jsonResponse);
                        waitingDialog.dismiss();
                        Toast.makeText(ProfileActivity.this, "Error: " + jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    waitingDialog.dismiss();
                    Log.v("GettingCUstomerData__", "Error e: " + e);
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                waitingDialog.dismiss();
                Log.v("GettingCUstomerData__", "ErrorListenerError: " + error);
            }
        });
        Volley.newRequestQueue(ProfileActivity.this).add(jsonStringRequest);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        showLoadingDialog("Please Wait while we get your data");

        updateProfileBtn = findViewById(R.id.updateBtn);
        phoneEdit = findViewById(R.id.phoneEditRegister);
        firstNameEdit = findViewById(R.id.fNameEditRegister);
        lastNameEdit = findViewById(R.id.lNameEditRegister);
        companyEditBilling = findViewById(R.id.companyEditBilling);
        companyEditShipping = findViewById(R.id.companyEditShipping);
        addressEditBilling = findViewById(R.id.addressEditBilling);
        addressEditShipping = findViewById(R.id.addressEditShipping);
        cityEditBilling = findViewById(R.id.cityEditBilling);
        cityEditShipping = findViewById(R.id.cityEditShipping);
        stateEditBilling = findViewById(R.id.stateEditBilling);
        stateEditShipping = findViewById(R.id.stateEditShipping);
        postCodeEditBilling = findViewById(R.id.postcodeEditBilling);
        postCodeEditShipping = findViewById(R.id.postcodeEditShipping);
        countryEditBilling = findViewById(R.id.countryEditBilling);
        countryEditShipping = findViewById(R.id.countryEditShipping);

        getCurrentCustomerData();

        updateProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (phoneEdit.getText().toString().isEmpty() || firstNameEdit.getText().toString().isEmpty() || lastNameEdit.getText().toString().isEmpty() || companyEditBilling.getText().toString().isEmpty() || companyEditShipping.getText().toString().isEmpty() || addressEditBilling.getText().toString().isEmpty() || addressEditShipping.getText().toString().isEmpty() || cityEditBilling.getText().toString().isEmpty() || cityEditShipping.getText().toString().isEmpty() || postCodeEditBilling.getText().toString().isEmpty() || postCodeEditShipping.getText().toString().isEmpty() || countryEditBilling.getText().toString().isEmpty() || countryEditShipping.getText().toString().isEmpty()) {
                    Snackbar.make(findViewById(android.R.id.content), "Please Fill in all the fields above first. Don't leave anything empty", Snackbar.LENGTH_LONG).show();
                    return;
                }

                showLoadingDialog("Please Wait while we update your Profile");
                waitingDialog.show();
                sendRequestNow();

            }
        });
    }

    private void sendRequestNow() {

        final String url = "https://insignum.hr/wp-json/users/updateCustomer";

        final int customerId = appShared.getInt("customerId", 0);

        if (customerId == 0) {
            Snackbar.make(findViewById(android.R.id.content), "You are not logged in right now.", Snackbar.LENGTH_LONG).show();
            return;
        }

        //OKHTTP
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String bodyy = String.format("{\n\"id\":\"%s\",\n\"first_name\":\"%s\",\"last_name\":\"%s\",\"billing_country\":\"%s\",\"billing\":{\"first_name\":\"%s\",\"last_name\":\"%s\",\"company\":\"%s\",\"address_1\":\"%s\",\"address_2\":\"\",\"city\":\"%s\",\"state\":\"%s\",\"postcode\":\"%s\",\"country\":\"%s\",\"phone\":\"%s\"},\"shipping\":{\"first_name\":\"%s\",\"last_name\":\"%s\",\"company\":\"%s\",\"address_1\":\"%s\",\"address_2\":\"\",\"city\":\"%s\",\"state\":\"%s\",\"postcode\":\"%s\",\"country\":\"%s\"}}", customerId, firstNameEdit.getText().toString(), lastNameEdit.getText().toString(), countryEditBilling.getText().toString(), firstNameEdit.getText().toString(), lastNameEdit.getText().toString(), companyEditBilling.getText().toString(), addressEditBilling.getText().toString(), cityEditBilling.getText().toString(), "", postCodeEditBilling.getText().toString(), countryEditBilling.getText().toString(), phoneEdit.getText().toString(), firstNameEdit.getText().toString(), lastNameEdit.getText().toString(), companyEditShipping.getText().toString(), addressEditShipping.getText().toString(), cityEditShipping.getText().toString(), "", postCodeEditShipping.getText().toString(), countryEditShipping.getText().toString());
                    OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody body = RequestBody.create(mediaType, bodyy);
                    Request request = new Request.Builder()
                            .url(url)
                            .post(body)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("User-Agent", "PostmanRuntime/7.20.1")
                            .addHeader("Accept", "*/*")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Postman-Token", "727d7fff-0556-4d19-a64d-bf1aa8490588,a8d76b22-94b5-4154-bd13-a217aff279b9")
                            .addHeader("Host", "insignum.hr")
                            //.addHeader("Accept-Encoding", "gzip, deflate")
                            .addHeader("Content-Length", "591")
                            .addHeader("Connection", "keep-alive")
                            .addHeader("cache-control", "no-cache")
                            //.build();
                            .build();

                    final Response response = client.newCall(request).execute();

                    String ress = response.body().string();

                    Log.v("RegistrationProgress__", "Message: " + ress);

                    if (!ress.isEmpty()) {
                        final JSONObject responseObject = new JSONObject(ress);

                        if (responseObject.has("id")) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ProfileActivity.this, "Profile Updated Successful.", Toast.LENGTH_SHORT).show();
                                    int id = responseObject.optInt("id");

                                    waitingDialog.dismiss();

                                    appSharedEditor.putInt("customerId", id);
                                    appSharedEditor.apply();

                                    Log.v("RegistrationProgress__", "ID Registered: " + id);

                                    finish();

//                        startActivity(new Intent(ProfileActivity.this, MainActivity.class));
//                        finish();
                                }
                            });

                        } else if (responseObject.has("message")) {
                            final String errorMessage = responseObject.optString("message");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ProfileActivity.this, "Error: " + errorMessage, Toast.LENGTH_SHORT).show();
                                    waitingDialog.dismiss();

                                    Log.v("RegistrationProgress__", "ResponseOKHTTP: " + response.isSuccessful());
                                    Log.v("RegistrationProgress__", "ResponseOKHTTPCode: " + response.code());

                                    waitingDialog.dismiss();
                                }
                            });
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    AlertDialog waitingDialog;

    private void showLoadingDialog(String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(ProfileActivity.this);
        //setting up the layout for alert dialog
        View view1 = LayoutInflater.from(ProfileActivity.this).inflate(R.layout.dialog_please_wait, null, false);

        builder1.setView(view1);

        TextView messageText = view1.findViewById(R.id.messageText);
        messageText.setText(message);

        waitingDialog = builder1.create();
    }
}
