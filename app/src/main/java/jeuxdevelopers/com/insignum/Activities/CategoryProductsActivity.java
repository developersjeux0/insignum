package jeuxdevelopers.com.insignum.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import jeuxdevelopers.com.insignum.Adapters.ProductAdapter;
import jeuxdevelopers.com.insignum.Models.CatModelInProducts;
import jeuxdevelopers.com.insignum.Models.ProductsModel;
import jeuxdevelopers.com.insignum.R;

public class CategoryProductsActivity extends AppCompatActivity {

    ImageView menuIcon;
    //NavigationViews
    DrawerLayout My_Drawer;

    int catId = 0;
    String catName = "";

    TextView catHeading;

    EditText searchEdit;

    private ImageView iv_Search;

    List<ProductsModel> list = new ArrayList<>();
    List<ProductsModel> tempList = new ArrayList<>();

    RecyclerView productsRecycler;
    ProductAdapter productAdapter;
    AVLoadingIndicatorView avi;

    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;
    int customerId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_products);

        catId = getIntent().getIntExtra("catId", 0);
        catName = getIntent().getStringExtra("catName");

        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        customerId = appShared.getInt("customerId", 0);

        initViews();
        Navigation_Work();

        hideSearch();

        iv_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchEdit.getVisibility() == View.VISIBLE) {
                    hideSearch();
                } else {
                    showSearch();
                }
            }
        });
    }

    private void initViews() {
        menuIcon = findViewById(R.id.menuIconMain);
        searchEdit = findViewById(R.id.searchEdit);
        catHeading = findViewById(R.id.categoryNameHeadingText);
        catHeading.setText(catName);

        productsRecycler = findViewById(R.id.productRecycler);
        iv_Search = findViewById(R.id.iv_Search);
        productAdapter = new ProductAdapter(list, this);
        productsRecycler.setLayoutManager(new LinearLayoutManager(this));
        productsRecycler.setItemAnimator(new DefaultItemAnimator());
        productsRecycler.setAdapter(productAdapter);

        checkData();

        avi = findViewById(R.id.avi);

        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterName(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void filterName(String name) {
        list.clear();
        productAdapter.notifyDataSetChanged();

        if (name.isEmpty()) {
            list.addAll(tempList);
            productAdapter.notifyDataSetChanged();
        } else {
            for (ProductsModel model : tempList) {
                if (model.getName().toLowerCase().contains(name.toLowerCase())) {
                    list.add(model);
                    productAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void checkData() {

        final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                Log.i("hello", "world");
                runOnUiThread(new Runnable() {
                    public void run() {

                        Log.v("Here)___", " Here 2");

                        if (MainActivity.allProducts.size() != 0) {

                            Log.v("Here)___", " Here 3");

                            if (list.size() == 0) {

                                Log.v("Here)___", " Here 4");

                                for (ProductsModel mo : MainActivity.allProducts) {

                                    for (CatModelInProducts catM : mo.getCategoryIds()) {
                                        if (catM.getCatId() == catId) {
                                            list.add(mo);
                                            tempList.add(mo);
                                        }
                                    }
                                }

                                //list.addAll(MainActivity.allProducts);
                                productAdapter.notifyDataSetChanged();
                                avi.setVisibility(View.GONE);

                                if (list.size() == 0) {
                                    findViewById(R.id.noProductCard).setVisibility(View.VISIBLE);
                                } else {
                                    findViewById(R.id.noProductCard).setVisibility(View.GONE);
                                }

                                scheduler.shutdownNow();

                            } else {
                                Log.v("Here)___", " Here 5");
                                scheduler.shutdownNow();
                            }
                        } else {

                        }
                    }
                });
            }
        }, 1, 3, TimeUnit.SECONDS);
    }

    private void Navigation_Work() {
        NavigationView navigation_View = findViewById(R.id.nav_view);
        My_Drawer = findViewById(R.id.drawerLayout);

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //      Toast.makeText(MainActivity.this, "Here", Toast.LENGTH_SHORT).show();
                if (My_Drawer.isDrawerOpen(Gravity.RIGHT)) {
                    My_Drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    My_Drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });

        TextView orderText = navigation_View.findViewById(R.id.ordersText);

        TextView home = navigation_View.findViewById(R.id.home);
        home.setOnClickListener(v -> {
            My_Drawer.closeDrawer(Gravity.RIGHT);
            startActivity(new Intent(CategoryProductsActivity.this, MainActivity.class));
            finish();
        });

        orderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CategoryProductsActivity.this, OrdersActivity.class));

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });
        TextView profileText = navigation_View.findViewById(R.id.profileText);
        profileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (customerId == 0) {
                    startActivity(new Intent(CategoryProductsActivity.this, LoginActivity.class));
                } else {
                    startActivity(new Intent(CategoryProductsActivity.this, ProfileActivity.class));
                }
//                if (FirebaseAuth.getInstance().getCurrentUser() == null){
//                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                    finish();
//                } else {
//                    startActivity(new Intent(MainActivity.this, ProfileActivity.class));
//                }

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        TextView about = navigation_View.findViewById(R.id.about);
        TextView contactUsText = navigation_View.findViewById(R.id.contactUsText);
        TextView about_us = navigation_View.findViewById(R.id.about_us);
        TextView privacyStatement = navigation_View.findViewById(R.id.privacyStatement);

        privacyStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                My_Drawer.closeDrawer(Gravity.RIGHT);
                startActivity(new Intent(CategoryProductsActivity.this, PrivacyStatementActivity.class));
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contactUsText.getVisibility() == View.VISIBLE) {
                    hideView(contactUsText);
                    hideView(about_us);
                } else {
                    showView(contactUsText);
                    showView(about_us);
                }
            }
        });

        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CategoryProductsActivity.this, AboutUsActivity.class));
                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        contactUsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(CategoryProductsActivity.this, ContactUsActivity.class));

//                if (FirebaseAuth.getInstance().getCurrentUser() == null){
//                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                    finish();
//                } else {
//                    startActivity(new Intent(MainActivity.this, ChatWithAdminActivity.class));
//                }

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        TextView checkStatusNav = navigation_View.findViewById(R.id.checkStatusNav);
        checkStatusNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(CategoryProductsActivity.this, OrderStatusCheckActivity.class));
            }
        });

        TextView trackStore = navigation_View.findViewById(R.id.textViewTrackShops);
        trackStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CategoryProductsActivity.this, ShowStoreLocationActivity.class));
            }
        });

        TextView logoutText = navigation_View.findViewById(R.id.logoutText);
        TextView webShop = navigation_View.findViewById(R.id.webShop);
        TextView profile = navigation_View.findViewById(R.id.profile);

        if (customerId == 0) {
            logoutText.setText("Login");
        } else {
            logoutText.setText("Logout");
        }

        webShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderText.getVisibility() == View.VISIBLE) {
                    hideView(orderText);
                } else {
                    showView(orderText);
                }
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profileText.getVisibility() == View.VISIBLE) {
                    hideView(profileText);
                    hideView(logoutText);
                } else {
                    showView(profileText);
                    showView(logoutText);
                }
            }
        });

        logoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (customerId == 0) {
                    startActivity(new Intent(CategoryProductsActivity.this, WelcomeScreen.class));
                    finish();
                } else {
                    appSharedEditor.putInt("customerId", 0);
                    appSharedEditor.apply();

                    startActivity(new Intent(CategoryProductsActivity.this, WelcomeScreen.class));
                    finish();

                    try {
                        My_Drawer.closeDrawer(Gravity.RIGHT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void hideSearch() {
        searchEdit.setText("");
        searchEdit.setVisibility(View.GONE);
        iv_Search.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_search));
    }

    private void showSearch() {
        searchEdit.setVisibility(View.VISIBLE);
        iv_Search.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_close));
    }

    private void hideView(TextView view) {
        view.setVisibility(View.GONE);
    }

    private void showView(TextView view) {
        view.setVisibility(View.VISIBLE);
    }
}
