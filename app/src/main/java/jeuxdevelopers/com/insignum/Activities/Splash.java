package jeuxdevelopers.com.insignum.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import jeuxdevelopers.com.insignum.R;

public class Splash extends AppCompatActivity {

    ImageView splashImage;

    LinearLayout splashLayout;

    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;

    int customerId = 0;

    boolean signInLater = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        customerId = appShared.getInt("customerId", 0);
        signInLater = appShared.getBoolean("signInLater", false);

        splashImage = findViewById(R.id.splashImage);
        splashLayout = findViewById(R.id.splashLayout);

        Animation move_and_fade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_and_fade);
        splashLayout.startAnimation(move_and_fade);

        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    if (customerId == 0) {
                        startActivity(new Intent(Splash.this, WelcomeScreen.class));
//                        if (!signInLater) {
//                            startActivity(new Intent(Splash.this, WelcomeScreen.class));
//                        } else {
//                            startActivity(new Intent(Splash.this, MainActivity.class));
//                        }
                    } else {
                        startActivity(new Intent(Splash.this, MainActivity.class));
                    }

                    finish();
                }
            }
        };
        timer.start();
    }
}
