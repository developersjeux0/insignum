package jeuxdevelopers.com.insignum.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import jeuxdevelopers.com.insignum.HelperClass.SendMail;
import jeuxdevelopers.com.insignum.R;

public class ContactUsActivity extends AppCompatActivity {

    EditText nameEdit, emailEdit, titleEdit, messageEdit;
    Button submitBtn;
    private CheckBox chkBox;
    private TextView tv_Agree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        initViews();
    }

    private void initViews() {
        nameEdit = findViewById(R.id.nameEditContact);
        emailEdit = findViewById(R.id.emailEditContact);
        titleEdit = findViewById(R.id.titleEditContact);
        messageEdit = findViewById(R.id.messageEditContact);
        submitBtn = findViewById(R.id.submitBtnContact);
        chkBox = findViewById(R.id.chkBox);
        tv_Agree = findViewById(R.id.tv_Agree);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nameEdit.getText().toString().isEmpty()) {
                    nameEdit.setError("Enter Your Name");
                    return;
                }

                if (emailEdit.getText().toString().isEmpty()) {
                    emailEdit.setError("Enter Your Email");
                    return;
                }

                if (titleEdit.getText().toString().isEmpty()) {
                    titleEdit.setError("Enter Your Subject Of Message");
                    return;
                }

                if (messageEdit.getText().toString().isEmpty()) {
                    messageEdit.setError("Enter Your Message");
                    return;
                }

                if (!chkBox.isChecked()) {
                    Toast.makeText(ContactUsActivity.this, "You agree to the terms of the Privacy Statement", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    String emailToSendReportTo = "info@insignum.hr";

                    SendMail sm = new SendMail(ContactUsActivity.this, emailToSendReportTo, "Contact From " + emailEdit.getText().toString(), nameEdit.getText().toString() + "\n" + messageEdit.getText().toString());
                    //Executing sendmail to send email
                    sm.execute();

                    Toast.makeText(ContactUsActivity.this, "Email Sent", Toast.LENGTH_SHORT).show();
                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tv_Agree.setOnClickListener(v -> {
            startActivity(new Intent(ContactUsActivity.this, PrivacyStatementActivity.class));
        });
    }
}
