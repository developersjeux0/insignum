package jeuxdevelopers.com.insignum.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import jeuxdevelopers.com.insignum.R;

public class AboutUsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_Mail1, tv_Location1, tv_Mail2, tv_Location2, tv_Mail3, tv_Location3;
    private ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us_actviity);
        linkViews();
        setClickListeners();
    }

    private void linkViews() {
        tv_Location1 = findViewById(R.id.tv_Location1);
        tv_Mail1 = findViewById(R.id.tv_Mail1);
        tv_Mail2 = findViewById(R.id.tv_Mail2);
        tv_Location2 = findViewById(R.id.tv_Location2);
        tv_Mail3 = findViewById(R.id.tv_Mail3);
        tv_Location3 = findViewById(R.id.tv_Location3);
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(this);
    }

    private void setClickListeners() {
        tv_Location1.setOnClickListener(this);
        tv_Mail1.setOnClickListener(this);
        tv_Mail2.setOnClickListener(this);
        tv_Location2.setOnClickListener(this);
        tv_Mail3.setOnClickListener(this);
        tv_Location3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_Mail1:
                openEmail("centar@insignum.hr");
                break;
            case R.id.tv_Location1:
                startActivity(new Intent(this, MapsActivity.class)
                        .putExtra("LAT", 45.8127875)
                        .putExtra("LON", 15.9781388)
                        .putExtra("TITLE", "LOKACIJA 1")
                        .putExtra("ADDRESS", "Kurelčeva 4, 10000 Zagreb")
                );
                break;
            case R.id.tv_Mail2:
                openEmail("centar@insignum.hr");
                break;
            case R.id.tv_Location2:
                startActivity(new Intent(this, MapsActivity.class)
                        .putExtra("LAT", 45.7939677)
                        .putExtra("LON", 15.9160208)
                        .putExtra("TITLE", "LOKACIJA 2")
                        .putExtra("ADDRESS", "Rudeška cesta 146, 10000 Zagreb")
                );
                break;
            case R.id.tv_Mail3:
                openEmail("vrbani@insignum.hr");
                break;
            case R.id.tv_Location3:
                startActivity(new Intent(this, MapsActivity.class)
                        .putExtra("LAT", 43.512204)
                        .putExtra("LON", 16.4352044)
                        .putExtra("TITLE", "LOKACIJA 3")
                        .putExtra("ADDRESS", "Ul. Petra Zoranića 1 (ulaz iz Svačićeve)21000 Split")
                );
                break;
            case R.id.backBtn:
                finish();
                break;
        }
    }

    private void openEmail(String email) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(intent, ""));
    }
}
