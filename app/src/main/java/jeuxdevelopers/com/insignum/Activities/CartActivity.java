package jeuxdevelopers.com.insignum.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.reginald.editspinner.EditSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jeuxdevelopers.com.insignum.Adapters.CartAdapter;
import jeuxdevelopers.com.insignum.Config.Config;
import jeuxdevelopers.com.insignum.Models.CartModel;
import jeuxdevelopers.com.insignum.Models.CartProductModel;
import jeuxdevelopers.com.insignum.Models.ProductsModel;
import jeuxdevelopers.com.insignum.R;
import jeuxdevelopers.com.insignum.Universal.APIsInfo;

public class CartActivity extends AppCompatActivity {

    RelativeLayout cartLayout, orderBookingLayout;

    RecyclerView cartRecycler;
    CartAdapter adapter;
    List<CartProductModel> list = new ArrayList<>();

    int customerId = 0;

    List<CartModel> cartModels = new ArrayList<>();

    Button checkOutBtn;

    TextView totalText;
    double totalAmount = 0;

    JSONObject billingObject;
    JSONObject shippingObject;

    String GETTINGCUSTOMERDATAURL = "";
    boolean isDBTChosed = false;
    boolean isCodChosed = false;
    boolean isCorvusChosed = false;

    //SharedPreferences
    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;

    public static SharedPreferences cart;
    public static SharedPreferences.Editor editor;

    ImageView menuIcon;
    //NavigationViews
    DrawerLayout My_Drawer;

    //setting and getting main activity
    private static CartActivity cartActivity;

    public static CartActivity getCartActivity() {
        return cartActivity;
    }

    private static void setCartActivity(CartActivity cartActivity) {
        CartActivity.cartActivity = cartActivity;
    }

    TextView headingText;


    //Instances Of BookingOrder Layout
    EditText ageEdit, nameEdit, addressEdit, cityEdit, zipCodeEdit, phoneEdit, emailEdit;
    EditSpinner countryEditSpinner;
    Button placeOrderBtn;

    RadioButton directBankRB, codRB, corvusRB;

    String[] PERMISSIONS = {};

    int PERMISSION_ALL = 1011;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        CartActivity.setCartActivity(this);

        String[] PERMISSIONS = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
                //android.Manifest.permission.CAMERA
        };

        this.PERMISSIONS = PERMISSIONS;

        initViews();

        headingText.setText("The Cart");
    }

    //Permission Request and has permission method
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1011: {
                if (grantResults.length > 0) {
                    String permissionsDenied = "";
                    for (String per : PERMISSIONS) {
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            permissionsDenied += "\n" + per;
                        }
                    }
                    // Show permissionsDenied

                    if (permissionsDenied.isEmpty()) {
                        Toast.makeText(cartActivity, "Permission Granter", Toast.LENGTH_SHORT).show();
                    } else {
                        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
                    }
                }
                return;
            }
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    Calendar ageCalender;

    private void initViews() {
        cart = getSharedPreferences("cart", MODE_PRIVATE);
        editor = cart.edit();

        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        menuIcon = findViewById(R.id.menuIconMain);
        Navigation_Work();

        showLoadingDialog();

        customerId = appShared.getInt("customerId", 0);

        cartRecycler = findViewById(R.id.cartRecycler);
        adapter = new CartAdapter(list, this);
        cartRecycler.setAdapter(adapter);
        cartRecycler.setLayoutManager(new LinearLayoutManager(this));

        totalText = findViewById(R.id.total_text);
        checkOutBtn = findViewById(R.id.checkoutBtn);

        cartLayout = findViewById(R.id.cartLayout);
        headingText = findViewById(R.id.headingTextCart);
        orderBookingLayout = findViewById(R.id.orderBookingLayout);
        cartLayout.setVisibility(View.VISIBLE);
        orderBookingLayout.setVisibility(View.GONE);

        //
        corvusRB = findViewById(R.id.corvusRB);
        directBankRB = findViewById(R.id.directBankRB);
        codRB = findViewById(R.id.codRB);
        ageEdit = findViewById(R.id.ageEdit);
        nameEdit = findViewById(R.id.nameEdit);
        countryEditSpinner = findViewById(R.id.countryEditSpinner);
        addressEdit = findViewById(R.id.addressEdit);
        cityEdit = findViewById(R.id.cityEdit);
        zipCodeEdit = findViewById(R.id.zipCodeEdit);
        phoneEdit = findViewById(R.id.phoneEdit);
        emailEdit = findViewById(R.id.emailEdit);
        placeOrderBtn = findViewById(R.id.placeOrderBtn);

        checkOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customerId == 0) {
                    startActivity(new Intent(CartActivity.this, LoginActivity.class));
                } else {
                    proceedToOrderBooking();
                }
            }
        });

        ageCalender = Calendar.getInstance();
        ageCalender.setTimeInMillis(946666800000L);
        final DatePickerDialog StartTime = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                ageCalender.set(year, monthOfYear, dayOfMonth);

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
                ageEdit.setText(dateFormat.format(ageCalender.getTime()));
            }

        }, ageCalender.get(Calendar.YEAR), ageCalender.get(Calendar.MONTH), ageCalender.get(Calendar.DAY_OF_MONTH));

        ageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartTime.show();
            }
        });

        placeOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ageEdit.getText().toString().isEmpty()) {
                    ageEdit.setError("Enter Your Age First");
                    return;
                }

                ageCalender.getTimeInMillis();

                long ageYearsMillis = System.currentTimeMillis() - ageCalender.getTimeInMillis();

                if (ageYearsMillis < 568025136000L) {
                    Snackbar.make(findViewById(android.R.id.content), "Your age is less than 18 years", Snackbar.LENGTH_LONG).show();
                    return;
                }

                if (nameEdit.getText().toString().isEmpty() || countryEditSpinner.getText().toString().isEmpty() || addressEdit.getText().toString().isEmpty() || cityEdit.getText().toString().isEmpty() || zipCodeEdit.getText().toString().isEmpty() || phoneEdit.getText().toString().isEmpty() || emailEdit.getText().toString().isEmpty()) {
                    Snackbar.make(findViewById(android.R.id.content), "Please Provide Billing Information First", Snackbar.LENGTH_LONG).show();
                    return;
                }

                if (!codRB.isChecked() && !directBankRB.isChecked() && !corvusRB.isChecked()) {
                    Snackbar.make(findViewById(android.R.id.content), "Please Select a Payment Method First", Snackbar.LENGTH_LONG).show();
                    return;
                }

                if (directBankRB.isChecked()) {
                    paymentMethodSelected = MainActivity.PaymentMethod.DIrectBankTransfer;
                } else if (codRB.isChecked()) {
                    paymentMethodSelected = MainActivity.PaymentMethod.CashOnDelivery;
                } else if (corvusRB.isChecked()) {
                    paymentMethodSelected = MainActivity.PaymentMethod.Corvus;
                }

                try {
                    billingObject.putOpt("first_name", nameEdit.getText().toString());
                    billingObject.putOpt("last_name", "");
                    billingObject.putOpt("country", countryEditSpinner.getText().toString());
                    billingObject.putOpt("address_1", addressEdit.getText().toString());
                    billingObject.putOpt("city", cityEdit.getText().toString());
                    billingObject.putOpt("postcode", zipCodeEdit.getText().toString());
                    billingObject.putOpt("phone", phoneEdit.getText().toString());
                    billingObject.putOpt("email", emailEdit.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (billingObject == null || shippingObject == null) {
                    Toast.makeText(CartActivity.this, "Your Data is not getted. Please restart this screen.", Toast.LENGTH_SHORT).show();
                } else {
                    waitingDialog.show();
                    try {
                        JSONObject orderObject = new JSONObject();
                        orderObject.put("set_paid", false);

                        switch (paymentMethodSelected) {
                            case Corvus:
                                orderObject.put("payment_method", "corvuspay");
                                orderObject.put("payment_method_title", "Plaćanje kreditnom karticom.");
                                break;
                            case DIrectBankTransfer:
                                orderObject.put("payment_method", "bacs");
                                orderObject.put("payment_method_title", "Plaćanje virmanom");
                                break;
                            case CashOnDelivery:
                                orderObject.put("payment_method", "cod");
                                orderObject.put("payment_method_title", "Plaćanje prilikom preuzimanja");
                                break;
                        }

                        orderObject.put("billing", billingObject);
                        orderObject.put("shipping", shippingObject);
                        //orderObject.put("customer_note", orderNoteString);
                        orderObject.put("customer_id", customerId);

                        JSONArray productsArray = new JSONArray();

                        for (CartProductModel model : list) {

                            JSONObject itemObject = new JSONObject();
                            itemObject.put("product_id", model.getId());
                            itemObject.put("quantity", model.getCartItemQuantity());

                            Log.v("OrderBooking__", "Product Added: " + model.getName() + " Quantity: " + model.getCartItemQuantity());

                            productsArray.put(itemObject);
                        }

                        orderObject.put("line_items", productsArray);

                        String ORDER_URL = "https://insignum.hr/wp-json/users/createOrder";

                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ORDER_URL, orderObject, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.has("id")) {
                                    //customerId

                                    int id = response.optInt("id");

                                    editor.clear();
                                    editor.apply();

                                    onResume();
                                    switch (paymentMethodSelected) {
                                        case DIrectBankTransfer:
                                            showDirectBackTransferDialog(id);
                                            waitingDialog.dismiss();
                                            break;
                                        case CashOnDelivery:
                                            MainActivity.getMainActivity().showSnackbar("Your Order is booked. Order ID: " + id + "\nTo Track, go to your orders page");
                                            waitingDialog.dismiss();
                                            finish();
                                            break;

                                        case Corvus:
                                            //MainActivity.getMainActivity().showSnackbar("Your Order is booked. Order ID: " + id + "\nTo Track, go to your orders page");
                                            currentOrderId = id;
                                            waitingDialog.dismiss();
                                            Pay_To_Get(new BigDecimal(totalAmount), "Order Id : " + id);
                                            break;
                                    }

//                            appSharedEditor.putInt("customerId", id);
//                            appSharedEditor.apply();

                                    Log.v("OrderBooking__", "Order Id: " + id);

                                } else {
                                    waitingDialog.dismiss();
                                    if (response.has("message")) {
                                        String message = response.optString("message");
                                        Toast.makeText(CartActivity.this, "Error: " + message, Toast.LENGTH_LONG).show();
                                        Log.v("OrderBooking__", "Error: " + response.toString());

                                    } else {
                                        Toast.makeText(CartActivity.this, "Error: " + response.toString(), Toast.LENGTH_SHORT).show();
                                        Log.v("OrderBooking__", "Error: " + response.toString());
                                    }
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                waitingDialog.dismiss();

                                Log.v("OrderBooking__", "Error In Listener: " + error);
                                Log.v("OrderBooking__", "Error Details: " + parseVolleyError(error));

                            }
                        });

                        request.setRetryPolicy(new DefaultRetryPolicy(
                                30000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                        Volley.newRequestQueue(CartActivity.this).add(request);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    int currentOrderId = 0;

    private void showDirectBackTransferDialog(final int orderIdString) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(CartActivity.this);
        //setting up the layout for alert dialog
        View view1 = LayoutInflater.from(CartActivity.this).inflate(R.layout.direct_back_transfer_order_layout, null, false);

        builder1.setView(view1);

        final AlertDialog dbtDialog = builder1.create();
        dbtDialog.setCancelable(false);
        dbtDialog.show();

        final RelativeLayout screenshotLayout = view1.findViewById(R.id.layoutToCapture);

        TextView orderIdText = view1.findViewById(R.id.orderIdText);
        orderIdText.setText(String.valueOf(orderIdString));

        TextView ibnNumberText = view1.findViewById(R.id.ibnNumberText);
        ibnNumberText.setText("4242 4242 4242 4242");

        Button takeScreenShotBtn = view1.findViewById(R.id.takeScreenshotBtn);
        Button copyClipboard = view1.findViewById(R.id.copyToClipboardBtn);
        Button finishBtn = view1.findViewById(R.id.finishBtn);

        copyClipboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("OrderID" + orderIdString, String.valueOf(orderIdString));
                clipboard.setPrimaryClip(clip);
                MainActivity.getMainActivity().showSnackbar("Order ID is Copied To Clipboard");
            }
        });

        takeScreenShotBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!hasPermissions(CartActivity.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(CartActivity.this, PERMISSIONS, PERMISSION_ALL);
                } else {
                    takeScreenshotToShare(screenshotLayout, "OrderId" + orderIdString);
                }
            }
        });

        finishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbtDialog.dismiss();
                finish();
            }
        });

    }

    AlertDialog waitingDialog;

    private void showLoadingDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(CartActivity.this);
        //setting up the layout for alert dialog
        View view1 = LayoutInflater.from(CartActivity.this).inflate(R.layout.dialog_please_wait, null, false);

        builder1.setView(view1);

        waitingDialog = builder1.create();
    }

    public static MainActivity.PaymentMethod paymentMethodSelected;

    private void proceedToOrderBooking() {

        cartLayout.setVisibility(View.GONE);
        orderBookingLayout.setVisibility(View.VISIBLE);

        nameEdit.setText(billingObject.optString("first_name") + " " + billingObject.optString("last_name"));
        countryEditSpinner.setText(billingObject.optString("country"));
        addressEdit.setText(billingObject.optString("address_1"));
        cityEdit.setText(billingObject.optString("city"));
        zipCodeEdit.setText(billingObject.optString("postcode"));
        phoneEdit.setText(billingObject.optString("phone"));
        emailEdit.setText(billingObject.optString("email"));
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        if (cartLayout.getVisibility() == View.VISIBLE) {
            super.onBackPressed();
        } else {
            orderBookingLayout.setVisibility(View.GONE);
            cartLayout.setVisibility(View.VISIBLE);
        }
    }

    public void refreshData() {
        onResume();
    }

    @Override
    protected void onResume() {
        super.onResume();

        totalAmount = 0;

        list.clear();
        cartModels.clear();

        int numberOfCartItems = cart.getInt("NoOfCartItems", 0);

        Log.v("CartItems__", "Number Of CartItems: " + numberOfCartItems);

        for (int j = 1; j <= numberOfCartItems; j++) {
            int getKey = cart.getInt("Cart_Item" + j, 0);
            int cartItemQuantity = cart.getInt("Cart_ItemQuantity" + j, 0);

            cartModels.add(new CartModel(numberOfCartItems, getKey, cartItemQuantity));
        }

        Log.v("CartItems__", "Models List Size: " + cartModels.size());

        for (CartModel mod : cartModels) {

            for (ProductsModel productModel : MainActivity.allProducts) {
                if (mod.getCart_Item() == productModel.getId()) {

                    CartProductModel model = new CartProductModel(productModel.getId(), productModel.getName(), productModel.getSlug(), productModel.getPermalink(), productModel.getType(), productModel.getStatus(), productModel.isFeatured(), productModel.getCatalog_visibility(), productModel.getDescription(), productModel.getShort_description(), productModel.getPrice(), productModel.getRegular_price(), productModel.getDate_on_sale_from(), productModel.getDate_on_sale_to(), productModel.isOn_sale(), productModel.isPurchasable(), productModel.getTotal_sales(), productModel.getParent_id(), productModel.getPurchase_note(), productModel.getCategoryIds(), productModel.getImageUrls(), productModel.getTagsList(), mod.getCart_Item(), mod.getCart_ItemQuantity());

                    boolean isAlreadyInList = false;

                    for (CartProductModel mm : list) {
                        if (mm.getId() == productModel.getId()) {
                            isAlreadyInList = true;
                        }
                    }

                    if (!isAlreadyInList) {

                        double proPrice = Double.parseDouble(model.getPrice());

                        totalAmount = totalAmount + (proPrice * (double) mod.getCart_ItemQuantity());

                        list.add(model);
                    }
                }
            }
        }

        Log.v("CartItems__", "Items List Size: " + list.size() + " TotalAmount: " + totalAmount);

        totalText.setText(String.valueOf(totalAmount) + " $");
        adapter.notifyDataSetChanged();

        customerId = appShared.getInt("customerId", 0);

        if (customerId != 0) {
            getCustomerData();
        }
    }

    private void getCustomerData() {
        if (billingObject == null || shippingObject == null) {
            //Snackbar.make(findViewById(android.R.id.content), "Getting Data", Snackbar.LENGTH_LONG).show();
            billingObject = new JSONObject();
            shippingObject = new JSONObject();

            GETTINGCUSTOMERDATAURL = "https://insignum.hr/wp-json/wc/v3/customers/" + customerId + APIsInfo.consumerKeyAndSecretString;

            final StringRequest jsonStringRequest = new StringRequest(Request.Method.GET, GETTINGCUSTOMERDATAURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonResponse = new JSONObject(response);

                        if (jsonResponse.has("id")) {

                            Snackbar.make(findViewById(android.R.id.content), "Welcome To Cart " + jsonResponse.optString("username"), Snackbar.LENGTH_LONG).show();

                            billingObject = jsonResponse.getJSONObject("billing");
                            shippingObject = jsonResponse.getJSONObject("shipping");

                            Log.v("GettingCUstomerData__", "Data IS Here: " + billingObject + "\n\n" + shippingObject);

                        } else if (jsonResponse.has("message")) {
                            Log.v("GettingCUstomerData__", "Error: " + jsonResponse);
                            Toast.makeText(CartActivity.this, "Error: " + jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.v("GettingCUstomerData__", "Error e: " + e);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("GettingCUstomerData__", "ErrorListenerError: " + error);
                }
            });
            Volley.newRequestQueue(CartActivity.this).add(jsonStringRequest);

        } else {
            Log.v("GettingCUstomerData__", "Data IS Here: " + billingObject + "\n\n" + shippingObject);
            Snackbar.make(findViewById(android.R.id.content), "Welcome To Cart " + billingObject.optString("username"), Snackbar.LENGTH_LONG).show();
        }
    }

    public static String parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");

            //Toast.makeText(RegisterActivity.this, "Error: " + , Toast.LENGTH_SHORT).show();

            Log.v("RegistrationProgress__", responseBody);
            JSONObject data = new JSONObject(responseBody);
            JSONArray errors = data.getJSONArray("errors");
            JSONObject jsonMessage = errors.getJSONObject(0);
            return jsonMessage.getString("message");
        } catch (Exception e) {
            return "";
        }
    }


    private File takeScreenshotToShare(RelativeLayout rl, String name) {

        try {
            // image naming and path  to include sd card  appending name you choose for file
            final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/InsignumData";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();
            File file = new File(dir, name + ".png");
            // create bitmap screen capture
            rl.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(rl.getDrawingCache());
//            Bitmap bitmap = Bitmap.createBitmap(
//                    scrollView.getChildAt(0).getWidth(),
//                    scrollView.getChildAt(0).getHeight(),
//                    Bitmap.Config.ARGB_8888);
//            Canvas c = new Canvas(bitmap);
//            scrollView.getChildAt(0).draw(c);
            //rl.setDrawingCacheEnabled(false);
            FileOutputStream outputStream = new FileOutputStream(file);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, 84, outputStream);
            outputStream.flush();
            outputStream.close();

            MediaScannerConnection.scanFile(CartActivity.this, new String[]{file.getPath()}, new String[]{"image/png"}, null);

            MainActivity.getMainActivity().showSnackbar("Screenshot Saved");

            //findViewById(R.id.headingAccStatement).setVisibility(View.GONE);
            return file;
        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
            return null;
        }
    }

    private final int Pay_Pal_Request_Code = 998;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(Config.PAYPAL_CLIENT_ID);

    private void Pay_To_Get(BigDecimal Value, String Data) {
        PayPalPayment Payment_Call = new PayPalPayment(Value, "USD", Data,
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent Intent_Call = new Intent(CartActivity.this, PaymentActivity.class);
        Intent_Call.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        Intent_Call.putExtra(PaymentActivity.EXTRA_PAYMENT, Payment_Call);
        startActivityForResult(Intent_Call, Pay_Pal_Request_Code);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Pay_Pal_Request_Code) {
            if (resultCode == RESULT_OK) {

                PaymentConfirmation Confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (Confirmation != null) {

                    Log.v("Paypal__", "PaypalResponse: " + Confirmation.toJSONObject().toString());

                    try {

                        JSONObject res = Confirmation.toJSONObject();

                        JSONObject responseObject = res.getJSONObject("response");
                        String paymentId = responseObject.optString("id");

                        Log.v("OrderTransactionData__", "PaymentId : " + paymentId);

                        waitingDialog.show();

                        String NEWURL = "https://insignum.hr/wp-json/users/transaction_id?payment_id=" + paymentId;

                        ProcessPayment(NEWURL);

                    } catch (Exception e) {
                        e.printStackTrace();

                        Toast.makeText(cartActivity, "Counldn't process the payment at this moment. Kindly go to your orders to process payment.", Toast.LENGTH_SHORT).show();
                        finish();

                        waitingDialog.dismiss();
                    }

                }
            } else if (resultCode == RESULT_CANCELED) {
                Snackbar.make(findViewById(android.R.id.content), "Payment Canceled!", Snackbar.LENGTH_LONG).show();
                Toast.makeText(cartActivity, "Counldn't process the payment at this moment. Kindly go to your orders to process payment.", Toast.LENGTH_SHORT).show();
                finish();
                waitingDialog.dismiss();

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Snackbar.make(findViewById(android.R.id.content), "Error Contact CS \nError: ", Snackbar.LENGTH_LONG).show();
                Toast.makeText(cartActivity, "Counldn't process the payment at this moment. Kindly go to your orders to process payment.", Toast.LENGTH_SHORT).show();
                finish();
                waitingDialog.dismiss();
            }
        }
    }

    private void ProcessPayment(String url) {

        Log.v("OrderTransactionData__", "Transaction Details getting started");

        final StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.v("OrderTransactionData__", "Transaction Details Getted");

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("transactions");

                    if (jsonArray.length() != 0) {
                        JSONObject transactionObject = jsonArray.getJSONObject(0);

                        JSONArray relatedResources = transactionObject.getJSONArray("related_resources");

                        if (relatedResources.length() != 0) {

                            JSONObject beforeSale = relatedResources.getJSONObject(0);

                            JSONObject sale = beforeSale.getJSONObject("sale");

                            String transactionId = sale.optString("id");
                            String datePaid = sale.optString("create_time");

                            Log.v("OrderTransactionData__", "Transaction ID : " + transactionId);

                            nowPushTransactionId(transactionId, datePaid);
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    waitingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                waitingDialog.dismiss();

                Log.v("OrderTransactionData__", "getting Error in LIstener: " + error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(CartActivity.this).add(request);
    }

    private void nowPushTransactionId(String transactionId, String datePaid) {

        Log.v("OrderTransactionData__", "PUshing starting");

        String orderUpdatingUrl = APIsInfo.startURL + "orders/" + currentOrderId + "" + APIsInfo.consumerKeyAndSecretString;

        JSONObject sendingObject = new JSONObject();
        try {
            sendingObject.put("set_paid", true);
            sendingObject.put("transaction_id", transactionId);
            sendingObject.put("date_paid", datePaid);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, orderUpdatingUrl, sendingObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.v("OrderTransactionData__", "Pushing Succesfull");

                if (response.has("id")) {
                    //customerId
                    Snackbar.make(findViewById(android.R.id.content), "Payment Successful!", Snackbar.LENGTH_LONG).show();

                    waitingDialog.dismiss();

                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                waitingDialog.dismiss();
                Log.v("OrderTransactionData__", "PUshing Error in LIstener: " + error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(CartActivity.this).add(request);
    }

    private void Navigation_Work() {
        NavigationView navigation_View = findViewById(R.id.nav_view);
        My_Drawer = findViewById(R.id.drawerLayout);

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //      Toast.makeText(MainActivity.this, "Here", Toast.LENGTH_SHORT).show();
                if (My_Drawer.isDrawerOpen(Gravity.RIGHT)) {
                    My_Drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    My_Drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });

        TextView orderText = navigation_View.findViewById(R.id.ordersText);

        TextView home = navigation_View.findViewById(R.id.home);
        home.setOnClickListener(v -> {
            My_Drawer.closeDrawer(Gravity.RIGHT);
            startActivity(new Intent(CartActivity.this, MainActivity.class));
            finish();
        });

        orderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CartActivity.this, OrdersActivity.class));

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });
        TextView profileText = navigation_View.findViewById(R.id.profileText);
        profileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (customerId == 0) {
                    startActivity(new Intent(CartActivity.this, LoginActivity.class));
                } else {
                    startActivity(new Intent(CartActivity.this, ProfileActivity.class));
                }
//                if (FirebaseAuth.getInstance().getCurrentUser() == null){
//                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                    finish();
//                } else {
//                    startActivity(new Intent(MainActivity.this, ProfileActivity.class));
//                }

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        TextView about = navigation_View.findViewById(R.id.about);
        TextView contactUsText = navigation_View.findViewById(R.id.contactUsText);
        TextView about_us = navigation_View.findViewById(R.id.about_us);
        TextView privacyStatement = navigation_View.findViewById(R.id.privacyStatement);

        privacyStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                My_Drawer.closeDrawer(Gravity.RIGHT);
                startActivity(new Intent(CartActivity.this, PrivacyStatementActivity.class));
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contactUsText.getVisibility() == View.VISIBLE) {
                    hideView(contactUsText);
                    hideView(about_us);
                } else {
                    showView(contactUsText);
                    showView(about_us);
                }
            }
        });

        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CartActivity.this, AboutUsActivity.class));
                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        contactUsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(CartActivity.this, ContactUsActivity.class));

//                if (FirebaseAuth.getInstance().getCurrentUser() == null){
//                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                    finish();
//                } else {
//                    startActivity(new Intent(MainActivity.this, ChatWithAdminActivity.class));
//                }

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        TextView checkStatusNav = navigation_View.findViewById(R.id.checkStatusNav);
        checkStatusNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(CartActivity.this, OrderStatusCheckActivity.class));
            }
        });

        TextView trackStore = navigation_View.findViewById(R.id.textViewTrackShops);
        trackStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CartActivity.this, ShowStoreLocationActivity.class));
            }
        });

        TextView logoutText = navigation_View.findViewById(R.id.logoutText);
        TextView webShop = navigation_View.findViewById(R.id.webShop);
        TextView profile = navigation_View.findViewById(R.id.profile);

        if (customerId == 0) {
            logoutText.setText("Login");
        } else {
            logoutText.setText("Logout");
        }

        webShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderText.getVisibility() == View.VISIBLE) {
                    hideView(orderText);
                } else {
                    showView(orderText);
                }
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profileText.getVisibility() == View.VISIBLE) {
                    hideView(profileText);
                    hideView(logoutText);
                } else {
                    showView(profileText);
                    showView(logoutText);
                }
            }
        });

        logoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (customerId == 0) {
                    startActivity(new Intent(CartActivity.this, WelcomeScreen.class));
                    finish();
                } else {
                    appSharedEditor.putInt("customerId", 0);
                    appSharedEditor.apply();

                    startActivity(new Intent(CartActivity.this, WelcomeScreen.class));
                    finish();

                    try {
                        My_Drawer.closeDrawer(Gravity.RIGHT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void hideView(TextView view) {
        view.setVisibility(View.GONE);
    }

    private void showView(TextView view) {
        view.setVisibility(View.VISIBLE);
    }
}
