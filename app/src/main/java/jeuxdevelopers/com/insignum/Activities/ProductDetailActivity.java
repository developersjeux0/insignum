package jeuxdevelopers.com.insignum.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import jeuxdevelopers.com.insignum.Adapters.ProductAdapter;
import jeuxdevelopers.com.insignum.Models.CartModel;
import jeuxdevelopers.com.insignum.Models.CatModelInProducts;
import jeuxdevelopers.com.insignum.Models.ProductsModel;
import jeuxdevelopers.com.insignum.R;

public class ProductDetailActivity extends AppCompatActivity {

    int productId = 0;
    ProductsModel selectedModel;
    TextView productNameHeading;

    ImageView productImage;

    TextView productName, productPrice, productStockQuantity, amountToBeAddedToCart, skuText;
    ImageView addQuantity, removeQuantity;

    RecyclerView relatedProductsRecycler;
    List<ProductsModel> relatedProducts = new ArrayList<>();
    ProductAdapter relatedProductsAdapte;

    TextView noRelatedProductsText, productDescriptionText;

    int quantityToBeAdded = 1;

    Button addToCartBtn, viewCartBtn;
    List<CartModel> cartItems = new ArrayList<>();
    RelativeLayout amountLayout;

    SharedPreferences cart;
    SharedPreferences.Editor editor;
    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;

    ImageView menuIcon;
    //NavigationViews
    DrawerLayout My_Drawer;

    int customerId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        productId = getIntent().getIntExtra("productId", 0);

        for (ProductsModel model : MainActivity.allProducts) {
            if (model.getId() == productId) {
                selectedModel = model;
            }
        }

        initViews();

        if (selectedModel == null) {
            Toast.makeText(this, "Unable to get Product Data", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        customerId = appShared.getInt("customerId", 0);

        menuIcon = findViewById(R.id.menuIconMain);
        Navigation_Work();

        productNameHeading.setText(selectedModel.getName());
        productName.setText(selectedModel.getName());
        productPrice.setText(selectedModel.getPrice() + " kn");
        productStockQuantity.setText(selectedModel.getStock_quantity() + " in stock");

        if (selectedModel.getStock_quantity() > 0) {
            quantityToBeAdded = 1;
            amountToBeAddedToCart.setText(String.valueOf(quantityToBeAdded));
        } else {
            quantityToBeAdded = 0;
            amountToBeAddedToCart.setText(String.valueOf(quantityToBeAdded));
        }

        try {
            Glide.with(this).load(selectedModel.getImageUrls().get(0)).into(productImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        addQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedModel.getStock_quantity() > 0) {

                    int newQuan = quantityToBeAdded + 1;

                    if (newQuan <= selectedModel.getStock_quantity()) {
                        quantityToBeAdded = newQuan;
                        amountToBeAddedToCart.setText(String.valueOf(quantityToBeAdded));
                    } else {
                        Toast.makeText(ProductDetailActivity.this, "This amount is not available in amount greater than this number", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(ProductDetailActivity.this, "The product isn't available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        removeQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quantityToBeAdded > 1) {

                    int newQuan = quantityToBeAdded - 1;
                    quantityToBeAdded = newQuan;

                    amountToBeAddedToCart.setText(String.valueOf(quantityToBeAdded));

                } else {
                    Toast.makeText(ProductDetailActivity.this, "Cannot Reduct the amount less than the current amount", Toast.LENGTH_SHORT).show();
                }
            }
        });

        String skuTextString = "SKU: " + selectedModel.getSku() + " Category: ";

        for (CatModelInProducts ccMm : selectedModel.getCategoryIds()) {

            skuTextString = skuTextString + ccMm.getCatName() + ", ";
        }

        productDescriptionText.setText(selectedModel.getDescription());

        skuText.setText(skuTextString);

        for (ProductsModel productsModelForRelated : MainActivity.allProducts) {

            if (selectedModel.getRelatedProductsIds().contains(productsModelForRelated.getId())) {
                relatedProducts.add(productsModelForRelated);
            }
        }

        if (relatedProducts.size() == 0) {
            noRelatedProductsText.setVisibility(View.VISIBLE);
        }

        findViewById(R.id.viewCartBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetailActivity.this, CartActivity.class));
            }
        });

        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (appShared.getInt("customerId", 0) == 0) {
                    Toast.makeText(ProductDetailActivity.this, "You must be registered to buy products", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (appShared.getBoolean("isBillingDataAvailable", false)) {
                    int numberOfCartItemss = cart.getInt("NoOfCartItems", 0);
                    for (int j = 1; j <= numberOfCartItemss; j++) {
                        int getKey = cart.getInt("Cart_Item" + j, 0);
                        int quantity = cart.getInt("Cart_ItemQuantity" + j, 0);
                        cartItems.add(new CartModel(numberOfCartItemss, getKey, quantity));
                    }

                    boolean isExists = false;

                    for (CartModel mm : cartItems) {
                        if (mm.getCart_Item() == selectedModel.getId()) {
                            isExists = true;
                        }
                    }

                    if (isExists) {
                        deleteandupdate();
                        checkCartIfEmpty();
                    } else {

                        if (quantityToBeAdded == 0) {
                            Toast.makeText(ProductDetailActivity.this, "Sorry the product isn't available at this moment. Try again later.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        cartItems.clear();
                        int numberOfCartItems = cart.getInt("NoOfCartItems", 0);
                        numberOfCartItems++;

                        editor.putInt("Cart_Item" + numberOfCartItems, selectedModel.getId());
                        editor.putInt("Cart_ItemQuantity" + numberOfCartItems, quantityToBeAdded);
                        editor.putInt("NoOfCartItems", numberOfCartItems);
                        editor.apply();
                        refreshData();
                        checkCartIfEmpty();
                    }
                } else {
                    startActivity(new Intent(ProductDetailActivity.this, GetBillingDataActivity.class));
                }
            }
        });

        checkCartIfEmpty();

        refreshData();
    }

    private void deleteandupdate() {
        try {
            Log.v("cartitemsize", "before deleting" + String.valueOf(cartItems.size()));
            for (int i = 1; i <= cartItems.size(); i++) {
                int check = cartItems.get(i - 1).getCart_Item();
                if (check == selectedModel.getId()) {
                    cartItems.remove(i - 1);
                }
            }
            Log.v("cartitemsize", "after deleting" + String.valueOf(cartItems.size()));
            editor.clear();
            editor.apply();
        } finally {

            for (int i = 1; i <= cartItems.size(); i++) {
                editor.putInt("Cart_Item" + i, cartItems.get(i - 1).getCart_Item());
                editor.putInt("Cart_ItemQuantity" + i, cartItems.get(i - 1).getCart_ItemQuantity());
            }
            editor.putInt("NoOfCartItems", cartItems.size());
            editor.apply();
            cartItems.clear();
            addToCartBtn.setText("Add To Cart");
            amountLayout.setVisibility(View.VISIBLE);
            refreshData();
        }
    }

    private void refreshData() {

        //addToCardBtn.setText("Add To Cart");
        int numberOfCartItems = cart.getInt("NoOfCartItems", 0);

        boolean isAlreadyInCart = false;

        for (int j = 1; j <= numberOfCartItems; j++) {
            int getKey = cart.getInt("Cart_Item" + j, 0);
            if (getKey == selectedModel.getId()) {
                isAlreadyInCart = true;
                addToCartBtn.setText("Remove From Cart");
                amountLayout.setVisibility(View.INVISIBLE);
            }
        }

        if (!isAlreadyInCart) {
            addToCartBtn.setText("Add To Cart");
            amountLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        refreshData();
    }

    private void initViews() {

        cart = getSharedPreferences("cart", MODE_PRIVATE);
        editor = cart.edit();
        productDescriptionText = findViewById(R.id.productDescriptionProductDetail);
        noRelatedProductsText = findViewById(R.id.noRelatedProductsText);
        noRelatedProductsText.setVisibility(View.GONE);
        productNameHeading = findViewById(R.id.productNameHeadingText);
        productImage = findViewById(R.id.productImageDetail);
        productName = findViewById(R.id.productNameProductDetail);
        productPrice = findViewById(R.id.productPriceProductDetail);
        productStockQuantity = findViewById(R.id.stockQuantityProductDetail);
        amountToBeAddedToCart = findViewById(R.id.amountToAddToCart);
        addQuantity = findViewById(R.id.addQuantity);
        removeQuantity = findViewById(R.id.removeQuantity);
        skuText = findViewById(R.id.skuTextProductDetail);
        addToCartBtn = findViewById(R.id.addToCartBtn);
        amountLayout = findViewById(R.id.amountLayout);
        viewCartBtn = findViewById(R.id.viewCartBtn);

        relatedProductsRecycler = findViewById(R.id.relatedProductsRecycler);
        relatedProductsAdapte = new ProductAdapter(relatedProducts, this);
        relatedProductsRecycler.setAdapter(relatedProductsAdapte);
        relatedProductsRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    private void Navigation_Work() {
        NavigationView navigation_View = findViewById(R.id.nav_view);
        My_Drawer = findViewById(R.id.drawerLayout);

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //      Toast.makeText(MainActivity.this, "Here", Toast.LENGTH_SHORT).show();
                if (My_Drawer.isDrawerOpen(Gravity.RIGHT)) {
                    My_Drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    My_Drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });

        TextView orderText = navigation_View.findViewById(R.id.ordersText);

        TextView home = navigation_View.findViewById(R.id.home);
        home.setOnClickListener(v -> {
            My_Drawer.closeDrawer(Gravity.RIGHT);
            startActivity(new Intent(ProductDetailActivity.this, MainActivity.class));
            finish();
        });

        orderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetailActivity.this, OrdersActivity.class));

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });
        TextView profileText = navigation_View.findViewById(R.id.profileText);
        profileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (customerId == 0) {
                    startActivity(new Intent(ProductDetailActivity.this, LoginActivity.class));
                } else {
                    startActivity(new Intent(ProductDetailActivity.this, ProfileActivity.class));
                }
//                if (FirebaseAuth.getInstance().getCurrentUser() == null){
//                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                    finish();
//                } else {
//                    startActivity(new Intent(MainActivity.this, ProfileActivity.class));
//                }

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        TextView about = navigation_View.findViewById(R.id.about);
        TextView contactUsText = navigation_View.findViewById(R.id.contactUsText);
        TextView about_us = navigation_View.findViewById(R.id.about_us);
        TextView privacyStatement = navigation_View.findViewById(R.id.privacyStatement);

        privacyStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                My_Drawer.closeDrawer(Gravity.RIGHT);
                startActivity(new Intent(ProductDetailActivity.this, PrivacyStatementActivity.class));
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contactUsText.getVisibility() == View.VISIBLE) {
                    hideView(contactUsText);
                    hideView(about_us);
                } else {
                    showView(contactUsText);
                    showView(about_us);
                }
            }
        });

        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetailActivity.this, AboutUsActivity.class));
                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        contactUsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ProductDetailActivity.this, ContactUsActivity.class));

//                if (FirebaseAuth.getInstance().getCurrentUser() == null){
//                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                    finish();
//                } else {
//                    startActivity(new Intent(MainActivity.this, ChatWithAdminActivity.class));
//                }

                My_Drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        TextView checkStatusNav = navigation_View.findViewById(R.id.checkStatusNav);
        checkStatusNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ProductDetailActivity.this, OrderStatusCheckActivity.class));
            }
        });

        TextView trackStore = navigation_View.findViewById(R.id.textViewTrackShops);
        trackStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductDetailActivity.this, ShowStoreLocationActivity.class));
            }
        });

        TextView logoutText = navigation_View.findViewById(R.id.logoutText);
        TextView webShop = navigation_View.findViewById(R.id.webShop);
        TextView profile = navigation_View.findViewById(R.id.profile);

        if (customerId == 0) {
            logoutText.setText("Login");
        } else {
            logoutText.setText("Logout");
        }

        webShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderText.getVisibility() == View.VISIBLE) {
                    hideView(orderText);
                } else {
                    showView(orderText);
                }
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profileText.getVisibility() == View.VISIBLE) {
                    hideView(profileText);
                    hideView(logoutText);
                } else {
                    showView(profileText);
                    showView(logoutText);
                }
            }
        });

        logoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (customerId == 0) {
                    startActivity(new Intent(ProductDetailActivity.this, WelcomeScreen.class));
                    finish();
                } else {
                    appSharedEditor.putInt("customerId", 0);
                    appSharedEditor.apply();

                    startActivity(new Intent(ProductDetailActivity.this, WelcomeScreen.class));
                    finish();

                    try {
                        My_Drawer.closeDrawer(Gravity.RIGHT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void checkCartIfEmpty() {
        int numberOfCartItemss = cart.getInt("NoOfCartItems", 0);
        if (numberOfCartItemss <= 0) {
            viewCartBtn.setVisibility(View.GONE);
        } else {
            viewCartBtn.setVisibility(View.VISIBLE);
        }
    }

    private void hideView(TextView view) {
        view.setVisibility(View.GONE);
    }

    private void showView(TextView view) {
        view.setVisibility(View.VISIBLE);
    }
}
