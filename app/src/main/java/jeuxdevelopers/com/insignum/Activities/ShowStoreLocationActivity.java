package jeuxdevelopers.com.insignum.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;

import java.util.ArrayList;
import java.util.List;

import jeuxdevelopers.com.insignum.R;

public class ShowStoreLocationActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private Location mLastLocation;
    private static String TAG = "GoogleMap____";
    private Marker My_Location;
    private boolean isTrackingStarted = false;
    private int currentTrack = 0; // 1 = Location1, 2 = Location2 and 3 = Location3
    private Button btn_shop1, btn_shop2, btn_shop3;
    private TextView Location1, Location2, Location3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_store_location);

        btn_shop1 = findViewById(R.id.btn_shop1);
        btn_shop2 = findViewById(R.id.btn_shop2);
        btn_shop3 = findViewById(R.id.btn_shop3);

        Location1 = findViewById(R.id.Location_1);
        Location2 = findViewById(R.id.Location_2);
        Location3 = findViewById(R.id.Location_3);


        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        settingListener();

        checkGps();

        if (!Check_Location_Permission()) {
            if (ContextCompat.checkSelfPermission(ShowStoreLocationActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || (ContextCompat.checkSelfPermission(ShowStoreLocationActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

                ActivityCompat.requestPermissions(ShowStoreLocationActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            }

        }

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        FloatingActionButton F_Button = findViewById(R.id.My_Location_id_Button);
        F_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (mLastLocation != null) {
                    LatLng currentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

                    if (My_Location != null) {
                        My_Location.remove();
                    }
                    My_Location = mMap.addMarker(new MarkerOptions().position(currentLocation).title("My Location!"));
                    CameraUpdate lngZoom = CameraUpdateFactory.newLatLngZoom(
                            currentLocation, 15);
                    mMap.moveCamera(lngZoom);
                }

            }
        });

    }


    private void settingListener() {
        findViewById(R.id.btn_shop1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentTrack == 1) {
                    btn_shop1.setText("Track");
                    isTrackingStarted = false;
                    mMap.clear();
                    currentTrack = 0;
                    setMarkerOnCurrentLocation();
                } else {
                    if (mLastLocation != null) {
                        mMap.clear();
                        currentTrack = 1;
                        btn_shop1.setText("Stop Tracking");
                        btn_shop2.setText("Track");
                        btn_shop3.setText("Track");
                        drawRoute(new LatLng(45.8127875, 15.9781388), "Location 1 Kurelčeva 4, 10000 Zagreb");
                        isTrackingStarted = true;
                    } else {
                        Toast.makeText(ShowStoreLocationActivity.this, "Searching for your location...", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        findViewById(R.id.btn_shop2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentTrack == 2) {
                    btn_shop2.setText("Track");
                    isTrackingStarted = false;
                    mMap.clear();
                    currentTrack = 0;
                    setMarkerOnCurrentLocation();
                } else {
                    if (mLastLocation != null) {
                        mMap.clear();
                        isTrackingStarted = true;
                        currentTrack = 2;
                        btn_shop1.setText("Track");
                        btn_shop2.setText("Stop Tracking");
                        btn_shop3.setText("Track");
                        drawRoute(new LatLng(45.7939677, 15.91602), "Location 2 Rudeška cesta 146, 10000 Zagreb");

                    } else {
                        Toast.makeText(ShowStoreLocationActivity.this, "Searching for your location...", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        findViewById(R.id.btn_shop3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentTrack == 3) {
                    btn_shop3.setText("Track");
                    isTrackingStarted = false;
                    mMap.clear();
                    currentTrack = 0;
                    setMarkerOnCurrentLocation();
                } else {
                    if (mLastLocation != null) {
                        mMap.clear();
                        isTrackingStarted = true;
                        currentTrack = 3;
                        btn_shop1.setText("Track");
                        btn_shop2.setText("Track");
                        btn_shop3.setText("Stop Tracking");
                        drawRoute(new LatLng(43.512204, 16.4352044), "Location 3 Ul. Petra Zoranića 1");

                    } else {
                        Toast.makeText(ShowStoreLocationActivity.this, "Searching for your location...", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    boolean Check_Location_Permission() {

        if (ContextCompat.checkSelfPermission(ShowStoreLocationActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(ShowStoreLocationActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i(TAG, "" + googleMap.toString());
        mMap = googleMap;


        if (Check_Location_Permission()) {
            mMap.setMyLocationEnabled(true);

            if (mMap != null) {
                mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location arg0) {
                        Location_Managment(arg0);

                        mLastLocation = arg0;
                        LatLng currentLocation = new LatLng(arg0.getLatitude(), arg0.getLongitude());


                        if (My_Location != null) {
                            My_Location.remove();
                        }
                        My_Location = mMap.addMarker(new MarkerOptions().position(currentLocation).title("My Location!"));
                        CameraUpdate lngZoom = CameraUpdateFactory.newLatLngZoom(
                                currentLocation, 15);
                        if (!isTrackingStarted) {
                            mMap.animateCamera(lngZoom);
                        }

                    }
                });
            }
        } else {

        }
    }


    private void Location_Managment(Location arg0) {
//        double Distance_Shop1 = distance(arg0.getLatitude(), arg0.getLongitude(), 45.8127875, 15.9781388); // 6603 KM
//        double Distance_Shop2 = distance(arg0.getLatitude(), arg0.getLongitude(), 45.7939677, 15.91602); // 6606
//        double Distance_Shop3 = distance(arg0.getLatitude(), arg0.getLongitude(), 43.512204, 16.4352044); //7008

        double Distance_Shop1 = getDistanceBetween(arg0.getLatitude(), arg0.getLongitude(), 45.8127875, 15.9781388); // 6603 KM
        double Distance_Shop2 = getDistanceBetween(arg0.getLatitude(), arg0.getLongitude(), 45.7939677, 15.91602); // 6606
        double Distance_Shop3 = getDistanceBetween(arg0.getLatitude(), arg0.getLongitude(), 43.512204, 16.4352044); //7008

        Log.v("CheckingDistance", "Distance_Shop1: " + Distance_Shop1);
        Log.v("CheckingDistance", "Distance_Shop2: " + Distance_Shop2);
        Log.v("CheckingDistance", "Distance_Shop3: " + Distance_Shop3);

        double Largest = largest(Distance_Shop1, Distance_Shop2, Distance_Shop3);
        double Smallest = smallest(Distance_Shop1, Distance_Shop2, Distance_Shop3);

        Location1.setText("");
        Location2.setText("");
        Location3.setText("");

        if (Largest == Distance_Shop1) {
            Location1.setText("Farthest\n" + (int) Distance_Shop1 + " KM");

        } else if (Largest == Distance_Shop2) {
            Location2.setText("Farthest\n" + (int) Distance_Shop2 + " KM");

        } else if (Largest == Distance_Shop3) {
            Location3.setText("Farthest\n" + (int) Distance_Shop3 + " KM");
        }
        if (Smallest == Distance_Shop1) {
            Location1.setText("Closest\n" + (int) Distance_Shop1 + " KM");

        } else if (Smallest == Distance_Shop2) {
            Location2.setText("Closest\n" + (int) Distance_Shop2 + " KM");

        } else if (Smallest == Distance_Shop3) {
            Location3.setText("Closest\n" + (int) Distance_Shop3 + " KM");
        }

        if (Location1.getText().equals("")) {
            Location1.setText("Second\n" + (int) Distance_Shop1 + " KM");

        } else if (Location2.getText().equals("")) {
            Location2.setText("Second\n" + (int) Distance_Shop2 + " KM");

        } else if (Location3.getText().equals("")) {
            Location3.setText("Second\n" + (int) Distance_Shop3 + " KM");
        }

    }


    public static double largest(double first, double second, double third) {
        double max = first;
        if (second > max) {
            max = second;
        }
        if (third > max) {
            max = third;
        }
        return max;
    }

    public static double smallest(double first, double second, double third) {
        double min = first;
        if (second < min) {
            min = second;
        }
        if (third < min) {
            min = third;
        }
        return min;
    }


    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);


    }

    public static Double getDistanceBetween(double lat1, double lon1, double lat2, double lon2) {
        float[] result = new float[1];
        Location.distanceBetween(lat1, lon1, lat2, lon2, result);
        return (double) result[0] / 1000;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(ShowStoreLocationActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();

                        mMap.setMyLocationEnabled(true);

                        if (mMap != null) {
                            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                                @Override
                                public void onMyLocationChange(Location arg0) {
                                    mLastLocation = arg0;
                                    LatLng currentLocation = new LatLng(arg0.getLatitude(), arg0.getLongitude());
                                    if (My_Location != null) {
                                        My_Location.remove();
                                    }
                                    My_Location = mMap.addMarker(new MarkerOptions().position(currentLocation).title("My Location!"));
                                    CameraUpdate lngZoom = CameraUpdateFactory.newLatLngZoom(
                                            currentLocation, 15);
                                    mMap.animateCamera(lngZoom);
                                }
                            });
                        }


                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    public void drawRoute(LatLng location, String address) {


        LatLng current = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());


        // mMap.addMarker(new MarkerOptions().position(current).title(address));
        mMap.addMarker(new MarkerOptions().position(location).title(address));

        LatLng zaragoza = new LatLng(location.longitude, location.latitude);

        //Define list to get all latlng for the route
        List<LatLng> path = new ArrayList();


        //Execute Directions API request
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyD3xw6oNEQx2fgshPGxnQ_ATQRjqcPmNf0")
                .build();
        String l = mLastLocation.getLatitude() + "," + mLastLocation.getLongitude();
        String e = location.latitude + "," + location.longitude;
        DirectionsApiRequest req = DirectionsApi.getDirections(context, l, e);
        try {
            DirectionsResult res = req.await();

            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs != null) {
                    for (int i = 0; i < route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j = 0; j < leg.steps.length; j++) {
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length > 0) {
                                    for (int k = 0; k < step.steps.length; k++) {
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getLocalizedMessage());
            Log.v(TAG, ex.getMessage());
        }

        //Draw the polyline
        if (path.size() > 0) {
            PolylineOptions opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(5);
            mMap.addPolyline(opts);
        }

        mMap.getUiSettings().setZoomControlsEnabled(true);

        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(zaragoza, 6));
    }

    private void checkGps() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void setMarkerOnCurrentLocation() {
        LatLng currentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

        if (My_Location != null) {
            My_Location.remove();
        }
        My_Location = mMap.addMarker(new MarkerOptions().position(currentLocation).title("My Location!"));
        CameraUpdate lngZoom = CameraUpdateFactory.newLatLngZoom(
                currentLocation, 15);
        mMap.moveCamera(lngZoom);
    }
}
