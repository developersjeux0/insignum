package jeuxdevelopers.com.insignum.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import jeuxdevelopers.com.insignum.R;
import jeuxdevelopers.com.insignum.Universal.APIsInfo;

public class LoginActivity extends AppCompatActivity {

    String LOGINURL = "";

    TextView signUpText;

    EditText emailEdit, passwordEdit;
    Button loginBtn;
    ProgressBar progressBar;

    SharedPreferences appShared;
    SharedPreferences.Editor appSharedEditor;

    private static LoginActivity loginActivity;

    JSONObject billingObject, shippingObject;

    public static LoginActivity getLoginActivity() {
        return loginActivity;
    }

    private static void setLoginActivity(LoginActivity loginActivity) {
        LoginActivity.loginActivity = loginActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginActivity.setLoginActivity(this);

        appShared = getSharedPreferences("appshared", MODE_PRIVATE);
        appSharedEditor = appShared.edit();

        showLoadingDialog();

        signUpText = findViewById(R.id.signUpText);

        progressBar = findViewById(R.id.progressBar);
        signUpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        emailEdit = findViewById(R.id.emailEdit);
        passwordEdit = findViewById(R.id.passwordEdit);

        loginBtn = findViewById(R.id.loginBtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (emailEdit.getText().toString().isEmpty()) {
                    emailEdit.setError("Provide Email");
                    return;
                }

                if (passwordEdit.getText().toString().isEmpty()) {
                    passwordEdit.setError("Password Here");
                    return;
                }

                //Start Logging In
                loginBtn.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);

                loginUser();
            }
        });
    }

    AlertDialog waitingDialog;

    private void showLoadingDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
        //setting up the layout for alert dialog
        View view1 = LayoutInflater.from(LoginActivity.this).inflate(R.layout.dialog_please_wait, null, false);

        builder1.setView(view1);

        waitingDialog = builder1.create();
    }

    private void loginUser() {

        LOGINURL = String.format("https://insignum.hr/wp-json/users/login?email=%s&password=%s", emailEdit.getText().toString(), passwordEdit.getText().toString());

        final StringRequest jsonStringRequestForProducts = new StringRequest(Request.Method.GET, LOGINURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject responseObject = new JSONObject(response);

                    JSONObject dataObject = responseObject.getJSONObject("data");
                    Log.v("CheckingLoginResponse", dataObject.toString());
                    String id = dataObject.optString("ID");
                    String userName = dataObject.optString("user_login");
                    String userEmail = dataObject.optString("user_email");

                    Log.v("LOGGININ__", "DataObject" + dataObject);


                    appSharedEditor.putInt("customerId", Integer.valueOf(id));
                    appSharedEditor.apply();

                    getCurrentCustomerData(id);

                } catch (Exception e) {
                    e.printStackTrace();

                    Log.v("LOGGININ__", "Exception: " + e);

                    Toast.makeText(LoginActivity.this, "Error: " + response, Toast.LENGTH_SHORT).show();

                    progressBar.setVisibility(View.GONE);
                    loginBtn.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                loginBtn.setVisibility(View.VISIBLE);
                Log.v("LOGGININ__", "ErrorListenerError: " + error);
            }
        });
        Volley.newRequestQueue(this).add(jsonStringRequestForProducts);
    }

    private void getCurrentCustomerData(String id) {

        String GETTINGCUSTOMERDATAURL = "https://afrogarm.co.uk/wp-json/wc/v3/customers/" + id + "?consumer_key=ck_112b54602019062f1b6d063d452fa05e52814255&consumer_secret=cs_822aadf5830f569b2c0697308fd0c9704e9e964e";

        billingObject = new JSONObject();
        shippingObject = new JSONObject();

        GETTINGCUSTOMERDATAURL = "https://insignum.hr/wp-json/wc/v3/customers/" + id + APIsInfo.consumerKeyAndSecretString;

        final StringRequest jsonStringRequest = new StringRequest(com.android.volley.Request.Method.GET, GETTINGCUSTOMERDATAURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    if (jsonResponse.has("id")) {

                        billingObject = jsonResponse.getJSONObject("billing");
                        shippingObject = jsonResponse.getJSONObject("shipping");

                        String address = billingObject.optString("address_1");
                        if (address.equalsIgnoreCase("null") || address.isEmpty()) {
                            appSharedEditor.putBoolean("isBillingDataAvailable", false);
                            appSharedEditor.apply();
                        }else {
                            appSharedEditor.putBoolean("isBillingDataAvailable", true);
                            appSharedEditor.apply();
                        }

                        Log.v("GettingCUstomerData__", "Data IS Here: " + billingObject + "\n\n" + shippingObject);

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();

                    } else if (jsonResponse.has("message")) {
                        Log.v("GettingCUstomerData__", "Error: " + jsonResponse);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    loginBtn.setVisibility(View.VISIBLE);
                    Log.v("GettingCUstomerData__", "Error e: " + e);
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                loginBtn.setVisibility(View.VISIBLE);
                Log.v("GettingCUstomerData__", "ErrorListenerError: " + error);
            }
        });
        Volley.newRequestQueue(LoginActivity.this).add(jsonStringRequest);
    }
}
