package jeuxdevelopers.com.insignum.Universal;

public class APIsInfo {

    public static String startURL = "https://insignum.hr/wp-json/wc/v3/";

    public static String consumerKeyAndSecretString = "?consumer_key=ck_55aa3f4a157322610059faf4beb4f61f3b08cc64&consumer_secret=cs_3e5dbc594f5543838dd6ed0dd96f41fa7ff89d0d";

    public static String GET_CATEGORIES = startURL + "products/categories" + consumerKeyAndSecretString + "&per_page=100";

    public static String GET_ALL_PRODUCTS = startURL + "products" + consumerKeyAndSecretString + "&per_page=100&page=";
}
