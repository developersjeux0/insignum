package jeuxdevelopers.com.insignum.networks;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static String BASE_URL = "https://insignum.hr/wp-json/";
    private static Retrofit sRetrofit = null;
    static final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(50, TimeUnit.SECONDS)
            .connectTimeout(50, TimeUnit.SECONDS)
            .build();

    public static ApiInterface createRetrofitService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL).client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();
        return retrofit.create(ApiInterface.class);
    }
}


/*app link*/
/*https://insignum.hr/wp-json/users/order_track?order_number=123&code=2311*/
