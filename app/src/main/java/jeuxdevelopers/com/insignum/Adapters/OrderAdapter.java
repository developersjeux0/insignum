package jeuxdevelopers.com.insignum.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.util.List;

import jeuxdevelopers.com.insignum.Activities.OrdersActivity;
import jeuxdevelopers.com.insignum.Models.OrderModel;
import jeuxdevelopers.com.insignum.R;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    List<OrderModel> list;
    Context context;

    public OrderAdapter(List<OrderModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderAdapter.ViewHolder holder, int position) {

        final OrderModel model = list.get(position);

        OrderProductsAdapter adapter = new OrderProductsAdapter(model.getProductsList(), context);
        holder.productsReycycler.setLayoutManager(new LinearLayoutManager(context));
        holder.productsReycycler.setAdapter(adapter);

        holder.orderNumber.setText(String.valueOf(model.getOrderNumber()));
        holder.orderStatus.setText(model.getStatus());
        holder.orderPaymentMethod.setText(model.getPaymentTitle());
        holder.orderPrice.setText(String.valueOf(model.getTotalOrderPrice()));
        holder.orderKey.setText(model.getOrderKey());
        holder.orderCreatedOn.setText(model.getDateCreated());

        if (holder.productsReycycler.getVisibility() == View.VISIBLE){
            holder.arrowImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_arrow_drop_up_white_24dp));
        } else {
            holder.arrowImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_arrow_drop_down_white_24dp));
        }

        holder.productEnablerCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.productsReycycler.getVisibility() == View.VISIBLE){
                    holder.productsReycycler.setVisibility(View.GONE);
                    holder.arrowImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_arrow_drop_down_white_24dp));
                } else {
                    holder.productsReycycler.setVisibility(View.VISIBLE);
                    holder.arrowImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_arrow_drop_up_white_24dp));
                }
            }
        });

        if (model.getPaymentMethod().equals("corvuspay")){
            if (model.getTransactionId().isEmpty()){
                holder.processPayment.setVisibility(View.VISIBLE);
            } else {
                holder.processPayment.setVisibility(View.GONE);
            }
        } else {
            holder.processPayment.setVisibility(View.GONE);
        }

        holder.processPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrdersActivity.getOrdersActivity().PayToGet(new BigDecimal(model.getTotalOrderPrice()), "Order Id : " + model.getOrderId(), model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView orderNumber, orderKey, orderCreatedOn, orderPrice, orderStatus, orderPaymentMethod;
        CardView productEnablerCard;
        ImageView arrowImage;

        Button processPayment;

        RecyclerView productsReycycler;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            processPayment = itemView.findViewById(R.id.process_payment_btn_order_card);
            orderNumber = itemView.findViewById(R.id.order_number_order_card);
            orderKey = itemView.findViewById(R.id.order_key_order_card);
            orderCreatedOn = itemView.findViewById(R.id.order_date_order_card);
            orderPrice = itemView.findViewById(R.id.order_price_order_card);
            orderStatus = itemView.findViewById(R.id.order_status_order_card);
            orderPaymentMethod = itemView.findViewById(R.id.order_payment_method_order_card);

            productEnablerCard = itemView.findViewById(R.id.product_card_order_card);
            arrowImage = itemView.findViewById(R.id.arrow_image_order_card);
            productsReycycler = itemView.findViewById(R.id.products_recycler_order_card);
        }
    }
}
