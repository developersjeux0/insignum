package jeuxdevelopers.com.insignum.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import jeuxdevelopers.com.insignum.Activities.ProductDetailActivity;
import jeuxdevelopers.com.insignum.Models.ProductsModel;
import jeuxdevelopers.com.insignum.R;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    List<ProductsModel> list;
    Context context;

    public ProductAdapter(List<ProductsModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductAdapter.ViewHolder holder, int position) {

        final ProductsModel model = list.get(position);
        holder.productName.setText(model.getName());
        try {

            Glide.with(context).load(model.getImageUrls().get(0)).addListener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                    holder.avi.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                    holder.avi.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.productImage);
        } catch (Exception e){
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ProductDetailActivity.class).putExtra("productId", model.getId()));
            }
        });

        holder.productPrice.setText(model.getPrice() + " kn");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView productImage;
        TextView productName;
        AVLoadingIndicatorView avi;
        TextView productPrice;

        LinearLayout bgLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            productImage = itemView.findViewById(R.id.productImageProductCard);
            productName = itemView.findViewById(R.id.productNameProductCard);
            productPrice = itemView.findViewById(R.id.productPriceProductCard);
            avi = itemView.findViewById(R.id.avi);
            bgLayout = itemView.findViewById(R.id.backgroundLayoutProductCard);
        }
    }
}
