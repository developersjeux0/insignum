package jeuxdevelopers.com.insignum.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import jeuxdevelopers.com.insignum.Activities.CartActivity;
import jeuxdevelopers.com.insignum.Activities.CategoryProductsActivity;
import jeuxdevelopers.com.insignum.Activities.ProductDetailActivity;
import jeuxdevelopers.com.insignum.Models.CartProductModel;
import jeuxdevelopers.com.insignum.R;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    List<CartProductModel> list;
    Context context;

    public CartAdapter(List<CartProductModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdapter.ViewHolder holder, int position) {

        final CartProductModel model = list.get(position);

        holder.productName.setText(model.getName());

        holder.productQuantity.setText(String.valueOf(model.getCartItemQuantity()));

        double proPrice = Double.parseDouble(model.getPrice());

        double totalPrice = (double) model.getCartItemQuantity() * proPrice;

        holder.productPrice.setText(String.valueOf(totalPrice));
        Glide.with(context).load(model.getImageUrls().get(0)).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(holder.productImage);

        holder.productSinglePrice.setText(String.valueOf(model.getPrice()));

        holder.bgCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ProductDetailActivity.class).putExtra("productId", model.getId()));
            }
        });

        holder.crossImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Log.v("cartitemsize", "before deleting" + String.valueOf(list.size()));
                    for (int i = 1; i <= list.size(); i++) {
                        int check = list.get(i - 1).getId();
                        if (check == model.getId()) {
                            list.remove(i - 1);
                        }
                    }
                    Log.v("cartitemsize", "after deleting" + String.valueOf(list.size()));
                    CartActivity.editor.clear();
                    CartActivity.editor.apply();
                } finally {

                    for (int i = 1; i <= list.size(); i++) {
                        CartActivity.editor.putInt("Cart_Item" + i, list.get(i - 1).getId());
                        CartActivity.editor.putInt("Cart_ItemQuantity" + i, list.get(i - 1).getCartItemQuantity());
                    }
                    CartActivity.editor.putInt("NoOfCartItems", list.size());
                    CartActivity.editor.apply();

                    notifyDataSetChanged();
                    //cartItems.clear();
                    //addToCartBtn.setText("Add To Cart");
                    //amountLayout.setVisibility(View.VISIBLE);
                    //refreshData();

                    CartActivity.getCartActivity().refreshData();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView productImage, crossImgBtn;
        TextView productName, productPrice, productQuantity, productSinglePrice;

        CardView bgCard ;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            crossImgBtn = itemView.findViewById(R.id.crossBtnCartCard);
            productImage = itemView.findViewById(R.id.product_image_cart_card);
            productName = itemView.findViewById(R.id.product_name_cart_card);
            productPrice = itemView.findViewById(R.id.product_price_cart_card);
            productQuantity = itemView.findViewById(R.id.product_quantity_cart_card);
            bgCard = itemView.findViewById(R.id.bgCardCartCard);
            productSinglePrice = itemView.findViewById(R.id.product_single_price_cart_card);
        }
    }
}
