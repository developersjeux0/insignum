package jeuxdevelopers.com.insignum.Adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import jeuxdevelopers.com.insignum.Models.OrderProductModel;
import jeuxdevelopers.com.insignum.R;

public class OrderProductsAdapter extends RecyclerView.Adapter<OrderProductsAdapter.ViewHolder> {

    List<OrderProductModel> list;
    Context context;

    public OrderProductsAdapter(List<OrderProductModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public OrderProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_product_card, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderProductsAdapter.ViewHolder holder, int position) {

        OrderProductModel model = list.get(position);
        holder.productName.setText(model.getProductName());
        holder.productPrice.setText(String.valueOf(model.getProductPrice()));
        holder.productQuantity.setText(String.valueOf(model.getQuantity()));
        holder.productTotalPrice.setText(String.valueOf(model.getProductTotalPrice()));

        holder.bgLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView productName, productPrice, productQuantity, productTotalPrice;
        RelativeLayout bgLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            bgLayout = itemView.findViewById(R.id.order_product_card_bg_layout);
            productName = itemView.findViewById(R.id.product_name_order_product_card);
            productPrice = itemView.findViewById(R.id.product_price_order_product_card);
            productQuantity = itemView.findViewById(R.id.product_quantity_order_product_card);
            productTotalPrice = itemView.findViewById(R.id.total_product_price_order_product_card);
        }
    }
}
