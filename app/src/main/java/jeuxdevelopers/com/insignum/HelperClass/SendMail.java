package jeuxdevelopers.com.insignum.HelperClass;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class SendMail extends AsyncTask<Void, Void, Void> {

    //Declaring Variables
    private Context context;
    private Session session;

    //Information to send email
    private String email;
    private String subject;
    private String messageText;
    private String path = "";

    //Progressdialog to show while sending email
    private ProgressDialog progressDialog;

    //Class Constructor
    public SendMail(Context context, String email, String subject, String messageText){
        //Initializing variables
        this.context = context;
        this.email = email;
        this.subject = subject;
        this.messageText = messageText;
        progressDialog = new ProgressDialog(context);
//        progressDialog.setMessage("Please Wait while we are sending The Email");
//        progressDialog.show();
    }

    public SendMail(Context context, String email, String subject, String messageText, String path){
        //Initializing variables
        this.context = context;
        this.email = email;
        this.subject = subject;
        this.messageText = messageText;
        this.path = path;
        progressDialog = new ProgressDialog(context);
//        progressDialog.setMessage("Please Wait while we are sending The Email");
//        progressDialog.show();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        progressDialog.setTitle("Sending Mail");
//        progressDialog.setMessage("Please Wait while we send email");
//        progressDialog.show();
        //Showing progress dialog while sending email
        //progressDialog = ProgressDialog.show(context,"Sending Report","Please wait...",false,false);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        //Dismissing the progress dialog
        //progressDialog.dismiss();
        //Showing a success message
        //Toast.makeText(context,"Report Sent",Toast.LENGTH_LONG).show();
        //ReportBugActivity.getReportBugActivity().finish();
    }

    @Override
    protected Void doInBackground(Void... params) {
        //Creating properties
        Properties props = new Properties();

        //Configuring properties for gmail
        //If you are not using gmail you may need to change the values
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        //Creating a new session
        session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    //Authenticating the password
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(ConfigEmailForReports.EMAIL, ConfigEmailForReports.PASSWORD);
                    }
                });

        try {

            if (path.isEmpty()){
                //Creating MimeMessage object
                MimeMessage mm = new MimeMessage(session);

                //Setting sender address
                mm.setFrom(new InternetAddress(ConfigEmailForReports.EMAIL));
                //Adding receiver
                mm.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
                //Adding subject
                mm.setSubject(subject);
                //Adding message
                mm.setText(messageText);

                //Sending email
                Transport.send(mm);

                Log.v("EmailSent: " , "EmailSent");

               // MDToast.makeText(context, "Email Sent", MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS).show();
            } else {
//                //Creating MimeMessage object
//                MimeMessage mm = new MimeMessage(session);
//
//                //Setting sender address
//                mm.setFrom(new InternetAddress(Config.EMAIL));
//                //Adding receiver
//                mm.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
//                //Adding subject
//                mm.setSubject(subject);
//                //Adding message
//                mm.setText(message);
//
//                DataSource source = new FileDataSource(path);
//
//                mm.setDataHandler(new DataHandler(source));
//
//                //Sending email
//                Transport.send(mm);
                // Create a default MimeMessage object.
//                Message message = new MimeMessage(session);
//
//                // Set From: header field of the header.
//                message.setFrom(new InternetAddress(ConfigEmailForReports.EMAIL));
//
//                // Set To: header field of the header.
//                message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
//
//                // Set Subject: header field
//                message.setSubject(subject);
//
//                // Create the message part
//                BodyPart messageBodyPart = new MimeBodyPart();
//
//                // Now set the actual message
//                messageBodyPart.setText(messageText);
//
//                // Create a multipar message
//                Multipart multipart = new MimeMultipart();
//
//                // Set text message part
//                multipart.addBodyPart(messageBodyPart);
//
//                // Part two is attachment
//                messageBodyPart = new MimeBodyPart();
//                String filename = path;
//                DataSource source = new FileDataSource(filename);
//                messageBodyPart.setDataHandler(new DataHandler(source));
//                messageBodyPart.setFileName(filename);
//                multipart.addBodyPart(messageBodyPart);
//
//                // Send the complete message parts
//                message.setContent(multipart);
//
//                // Send message
//                Transport.send(message);
//                Log.v("EmailSent: " , "EmailSent");
               //MDToast.makeText(context, "Email Sent", MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS).show();
            }

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
