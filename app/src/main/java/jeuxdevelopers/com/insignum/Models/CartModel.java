package jeuxdevelopers.com.insignum.Models;

public class CartModel {

    private int NoOfCartItems, Cart_Item, Cart_ItemQuantity;

    public CartModel(int noOfCartItems, int cart_Item, int cart_ItemQuantity) {
        NoOfCartItems = noOfCartItems;
        Cart_Item = cart_Item;
        Cart_ItemQuantity = cart_ItemQuantity;
    }

    public CartModel() {
    }

    public int getNoOfCartItems() {
        return NoOfCartItems;
    }

    public void setNoOfCartItems(int noOfCartItems) {
        NoOfCartItems = noOfCartItems;
    }

    public int getCart_Item() {
        return Cart_Item;
    }

    public void setCart_Item(int cart_Item) {
        Cart_Item = cart_Item;
    }

    public int getCart_ItemQuantity() {
        return Cart_ItemQuantity;
    }

    public void setCart_ItemQuantity(int cart_ItemQuantity) {
        Cart_ItemQuantity = cart_ItemQuantity;
    }
}
