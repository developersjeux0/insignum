package jeuxdevelopers.com.insignum.Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductsModel implements Serializable {

    private int id;
    private String name, slug, permalink, type, status, catalog_visibility, description, short_description, price, regular_price, date_on_sale_from, date_on_sale_to;
    private boolean on_sale, purchasable, featured;
    private int total_sales, parent_id;
    private String purchase_note, stock_status, sku;
    private int stock_quantity;
    private boolean manage_stock;

    private List<String> tagsList = new ArrayList<>();
    private List<String> imageUrls= new ArrayList<>();
    private List<CatModelInProducts> categoryIds= new ArrayList<>();
    private List<Integer> relatedProductsIds = new ArrayList<>();

    public ProductsModel(int id, String name, String slug, String permalink, String type, String status, boolean featured, String catalog_visibility, String description, String short_description, String price, String regular_price, String date_on_sale_from, String date_on_sale_to, boolean on_sale, boolean purchasable, int total_sales, int parent_id, String purchase_note, List<CatModelInProducts> categoryIds, List<String> imageUrls, List<String> tagsList, boolean manage_stock, String stock_status, int stock_quantity, String sku, List<Integer> productsIds) {
        this.tagsList = tagsList;
        this.imageUrls = imageUrls;
        this.stock_quantity = stock_quantity;
        this.manage_stock = manage_stock;
        this.stock_status = stock_status;
        this.id = id;
        this.name = name;
        this.sku = sku;
        this.relatedProductsIds = productsIds;
        this.slug = slug;
        this.permalink = permalink;
        this.type = type;
        this.status = status;
        this.featured = featured;
        this.catalog_visibility = catalog_visibility;
        this.description = description;
        this.short_description = short_description;
        this.price = price;
        this.regular_price = regular_price;
        this.date_on_sale_from = date_on_sale_from;
        this.date_on_sale_to = date_on_sale_to;
        this.on_sale = on_sale;
        this.purchasable = purchasable;
        this.total_sales = total_sales;
        this.parent_id = parent_id;
        this.purchase_note = purchase_note;
        this.categoryIds = categoryIds;
    }

    public ProductsModel() {
    }

    public List<Integer> getRelatedProductsIds() {
        return relatedProductsIds;
    }

    public void setRelatedProductsIds(List<Integer> relatedProductsIds) {
        this.relatedProductsIds = relatedProductsIds;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public boolean isManage_stock() {
        return manage_stock;
    }

    public void setManage_stock(boolean manage_stock) {
        this.manage_stock = manage_stock;
    }

    public String getStock_status() {
        return stock_status;
    }

    public void setStock_status(String stock_status) {
        this.stock_status = stock_status;
    }

    public int getStock_quantity() {
        return stock_quantity;
    }

    public void setStock_quantity(int stock_quantity) {
        this.stock_quantity = stock_quantity;
    }

    public List<String> getTagsList() {
        return tagsList;
    }

    public void setTagsList(List<String> tagsList) {
        this.tagsList = tagsList;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCatalog_visibility() {
        return catalog_visibility;
    }

    public void setCatalog_visibility(String catalog_visibility) {
        this.catalog_visibility = catalog_visibility;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRegular_price() {
        return regular_price;
    }

    public void setRegular_price(String regular_price) {
        this.regular_price = regular_price;
    }

    public String getDate_on_sale_from() {
        return date_on_sale_from;
    }

    public void setDate_on_sale_from(String date_on_sale_from) {
        this.date_on_sale_from = date_on_sale_from;
    }

    public String getDate_on_sale_to() {
        return date_on_sale_to;
    }

    public void setDate_on_sale_to(String date_on_sale_to) {
        this.date_on_sale_to = date_on_sale_to;
    }

    public boolean isOn_sale() {
        return on_sale;
    }

    public void setOn_sale(boolean on_sale) {
        this.on_sale = on_sale;
    }

    public boolean isPurchasable() {
        return purchasable;
    }

    public void setPurchasable(boolean purchasable) {
        this.purchasable = purchasable;
    }

    public int getTotal_sales() {
        return total_sales;
    }

    public void setTotal_sales(int total_sales) {
        this.total_sales = total_sales;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getPurchase_note() {
        return purchase_note;
    }

    public void setPurchase_note(String purchase_note) {
        this.purchase_note = purchase_note;
    }

    public List<CatModelInProducts> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<CatModelInProducts> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }
}
