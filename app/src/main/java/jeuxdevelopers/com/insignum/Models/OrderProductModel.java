package jeuxdevelopers.com.insignum.Models;

import java.io.Serializable;

public class OrderProductModel implements Serializable {

    private int id;
    private String productName;
    private int variationId, quantity;
    private String productPrice, productTotalPrice;

    public OrderProductModel(int id, String productName, int variationId, int quantity, String productPrice, String productTotalPrice) {
        this.id = id;
        this.productName = productName;
        this.variationId = variationId;
        this.quantity = quantity;
        this.productPrice = productPrice;
        this.productTotalPrice = productTotalPrice;
    }

    public OrderProductModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getVariationId() {
        return variationId;
    }

    public void setVariationId(int variationId) {
        this.variationId = variationId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductTotalPrice() {
        return productTotalPrice;
    }

    public void setProductTotalPrice(String productTotalPrice) {
        this.productTotalPrice = productTotalPrice;
    }
}
