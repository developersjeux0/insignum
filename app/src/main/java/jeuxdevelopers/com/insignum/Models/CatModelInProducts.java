package jeuxdevelopers.com.insignum.Models;

public class CatModelInProducts {

    private int catId;
    private String catName;

    public CatModelInProducts(int catId, String catName) {
        this.catId = catId;
        this.catName = catName;
    }

    public CatModelInProducts() {
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }
}
