package jeuxdevelopers.com.insignum.Models;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OrderModel {

    private int orderId;
    private String orderNumber, orderKey, status, currency, dateCreated, totalOrderPrice;
    private int customerId;
    private JSONObject billingObject, shippingObject;
    private String paymentMethod, paymentTitle, transactionId, datePaid;

    private List<OrderProductModel> productsList = new ArrayList<>();

    public OrderModel(int orderId, String orderNumber, String orderKey, String status, String currency, String dateCreated, String totalOrderPrice, int customerId, JSONObject billingObject, JSONObject shippingObject, String paymentMethod, String paymentTitle, String transactionId, String datePaid, List<OrderProductModel> productsList) {
        this.orderId = orderId;
        this.orderNumber = orderNumber;
        this.orderKey = orderKey;
        this.status = status;
        this.currency = currency;
        this.dateCreated = dateCreated;
        this.totalOrderPrice = totalOrderPrice;
        this.customerId = customerId;
        this.billingObject = billingObject;
        this.shippingObject = shippingObject;
        this.paymentMethod = paymentMethod;
        this.paymentTitle = paymentTitle;
        this.transactionId = transactionId;
        this.datePaid = datePaid;
        this.productsList = productsList;
    }

    public OrderModel() {
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderKey() {
        return orderKey;
    }

    public void setOrderKey(String orderKey) {
        this.orderKey = orderKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTotalOrderPrice() {
        return totalOrderPrice;
    }

    public void setTotalOrderPrice(String totalOrderPrice) {
        this.totalOrderPrice = totalOrderPrice;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public JSONObject getBillingObject() {
        return billingObject;
    }

    public void setBillingObject(JSONObject billingObject) {
        this.billingObject = billingObject;
    }

    public JSONObject getShippingObject() {
        return shippingObject;
    }

    public void setShippingObject(JSONObject shippingObject) {
        this.shippingObject = shippingObject;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentTitle() {
        return paymentTitle;
    }

    public void setPaymentTitle(String paymentTitle) {
        this.paymentTitle = paymentTitle;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(String datePaid) {
        this.datePaid = datePaid;
    }

    public List<OrderProductModel> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<OrderProductModel> productsList) {
        this.productsList = productsList;
    }
}
